<?php





$byp_pixelate_data 			= get_option('byp_pixelate_data') ? get_option('byp_pixelate_data') : array('0' => '');

$byp_border_data = get_option('byp_border_data');



$byp_border_r 			= $byp_border_data['byp_border_r'] ? $byp_border_data['byp_border_r'] : '';

$byp_border_g 			= $byp_border_data['byp_border_g'] ? $byp_border_data['byp_border_g'] : '';

$byp_border_b 			= $byp_border_data['byp_border_b'] ? $byp_border_data['byp_border_b'] : '';

$byp_pixelate_data			= !empty($byp_pixelate_data) ? $byp_pixelate_data : array('0' => '');



?>

 	
 	<?php

	// Success message

	if( isset($_GET['settings-updated']) && $_GET['settings-updated'] == 'true' ) {

		echo '<div id="message" class="updated notice notice-success is-dismissible">

				<p><strong>'.__("Your changes saved successfully.", "button-width-style").'</strong></p>

			  </div>';

	}

	?>



	<form action="options.php" method="POST" id="bwswpos-personal-settings-form" class="bwswpos-settings-form">

		

	<?php

	    settings_fields( 'byp_pix_options' );

	?>
	<div class="byp-sett-title byp-pixelate-title"><?php _e('Pixelate Options', 'buttons-with-style'); ?></div>

	<table class="form-table byp-group-button-sett">
		
		<tr>
			<th><?php _e('Num	', 'buttons-with-style');?></th>
			<th><?php _e('Color name	', 'buttons-with-style');?></th>
			<th><?php _e('Red', 'buttons-with-style');?></th>
			<th><?php _e('Green', 'buttons-with-style');?></th>
			<th><?php _e('Blue', 'buttons-with-style');?></th>
			<th><?php _e('Visual', 'buttons-with-style');?></th>
			<th class="color_properties"><?php _e('Color Properties', 'buttons-with-style');?></th>
			<th><?php _e('In Stock', 'buttons-with-style');?></th>
			<th><?php _e('2x8 H', 'buttons-with-style');?></th>
			<th><?php _e('2x8 V', 'buttons-with-style');?></th>
			<th><?php _e('2x6 H', 'buttons-with-style');?></th>
			<th><?php _e('2x6 V', 'buttons-with-style');?></th>
			<th><?php _e('1x8 H', 'buttons-with-style');?></th>
			<th><?php _e('1x8 V', 'buttons-with-style');?></th>
			<th><?php _e('1x6 H', 'buttons-with-style');?></th>
			<th><?php _e('1x6 V', 'buttons-with-style');?></th>
			<th><?php _e('2x4 H', 'buttons-with-style');?></th>
			<th><?php _e('2x4 V', 'buttons-with-style');?></th>
			<th><?php _e('2x2', 'buttons-with-style');?></th>
			<th><?php _e('1x4 H', 'buttons-with-style');?></th>
			<th><?php _e('1x4 V', 'buttons-with-style');?></th>
			<th><?php _e('1x2 H', 'buttons-with-style');?></th>
			<th><?php _e('1x2 V', 'buttons-with-style');?></th>
			<th><?php _e('1x1', 'buttons-with-style');?></th>	
			<th class="byp-act-head"><?php _e('Action', 'buttons-with-style');?></th>
		</tr>
		

	<?php if(!empty($byp_pixelate_data)) {

		foreach ($byp_pixelate_data as $grp_key => $grp_val) {



			$byp_clr_name = isset($grp_val['byp_clr_name']) 	? $grp_val['byp_clr_name'] 		: '';

		?>



		<tr valign="top" class="byp-group-btn-row" id="<?php echo $grp_key; ?>_byp_row" data-key="<?php echo $grp_key; ?>">

			<td> <?php echo $grp_key; ?></td>

			<td>							

				<input type="text" name="byp_pixelate_data[<?php echo $grp_key; ?>][byp_clr_name]" value="<?php echo $byp_clr_name; ?>" class="large-text byp_80" />			

			</td>

			<td>

				<input type="number" name="byp_pixelate_data[<?php echo $grp_key; ?>][byp_red]" value="<?php echo $grp_val['byp_red']; ?>" class="byp_red large-text byp_50" />				

			</td>

			<td>

				<input type="number" name="byp_pixelate_data[<?php echo $grp_key; ?>][byp_green]" value="<?php echo $grp_val['byp_green']; ?>" class="byp_green byp_50 large-text" />				

			</td>

			<td>

				<input type="number" name="byp_pixelate_data[<?php echo $grp_key; ?>][byp_blue]" value="<?php echo $grp_val['byp_blue']; ?>" class="byp_blue byp_50 large-text" />				

			</td>

			<td>

				<input type="hidden" name="byp_pixelate_data[<?php echo $grp_key; ?>][byp_visual]" value="<?php echo $grp_val['byp_visual']; ?>" class="byp_visual byp_50 large-text" />	 

				<?php 

				$bg = $grp_val['byp_visual'] ? $grp_val['byp_visual'] : '';

				?>

				<div class="byp_visual_div byp_50" style="background-color: <?php echo $bg; ?>"></div>

						

			</td>

			<td class="byp_71 check-grayscale">

				<input type="checkbox" name="byp_pixelate_data[<?php echo $grp_key; ?>][byp_fullclr]" value="byp_fullclr" <?php echo checked($grp_val['byp_fullclr'], 'byp_fullclr'); ?>>full color</input><br>

				<input type="checkbox" name="byp_pixelate_data[<?php echo $grp_key; ?>][byp_grayscale]" value="byp_grayscale" <?php echo checked($grp_val['byp_grayscale'], 'byp_grayscale'); ?>>grayscale</input>

			</td>

			<td class="byp_50">

				<input type="checkbox" name="byp_pixelate_data[<?php echo $grp_key; ?>][byp_stock]" value="byp_stock" <?php echo checked($grp_val['byp_stock'], 'byp_stock'); ?> ></input>

			</td>



			<!-- start bricks data -->

			<td class="byp_52">				

				<input type="number" name="byp_pixelate_data[<?php echo $grp_key; ?>][byp_2_8_h]" value="<?php echo $grp_val['byp_2_8_h']; ?>" class="byp_50 byp_max_50 large-text" /><span>%</span>				

			</td>

			<td class="byp_52">				

				<input type="number" name="byp_pixelate_data[<?php echo $grp_key; ?>][byp_2_8_v]" value="<?php echo $grp_val['byp_2_8_v']; ?>" class="byp_50 byp_max_50 large-text" /><span>%</span>				

			</td>

			<td class="byp_52">				

				<input type="number" name="byp_pixelate_data[<?php echo $grp_key; ?>][byp_2_6_h]" value="<?php echo $grp_val['byp_2_6_h']; ?>" class="byp_50 byp_max_50 large-text" /><span>%</span>				

			</td>

			<td class="byp_52">				

				<input type="number" name="byp_pixelate_data[<?php echo $grp_key; ?>][byp_2_6_v]" value="<?php echo $grp_val['byp_2_6_v']; ?>" class="byp_50 byp_max_50 large-text" /><span>%</span>				

			</td>

			<td class="byp_52">				

				<input type="number" name="byp_pixelate_data[<?php echo $grp_key; ?>][byp_1_8_h]" value="<?php echo $grp_val['byp_1_8_h']; ?>" class="byp_50 byp_max_50 large-text" /><span>%</span>				

			</td>

			<td class="byp_52">				

				<input type="number" name="byp_pixelate_data[<?php echo $grp_key; ?>][byp_1_8_v]" value="<?php echo $grp_val['byp_1_8_v']; ?>" class="byp_50 byp_max_50 large-text" /><span>%</span>				

			</td>

			<td class="byp_52">				

				<input type="number" name="byp_pixelate_data[<?php echo $grp_key; ?>][byp_1_6_h]" value="<?php echo $grp_val['byp_1_6_h']; ?>" class="byp_50 byp_max_50 large-text" /><span>%</span>				

			</td>

			<td class="byp_52">				

				<input type="number" name="byp_pixelate_data[<?php echo $grp_key; ?>][byp_1_6_v]" value="<?php echo $grp_val['byp_1_6_v']; ?>" class="byp_50 byp_max_50 large-text" /><span>%</span>				

			</td>

			<td class="byp_52">				

				<input type="number" name="byp_pixelate_data[<?php echo $grp_key; ?>][byp_2_4_h]" value="<?php echo $grp_val['byp_2_4_h']; ?>" class="byp_50 byp_max_50 large-text" /><span>%</span>				

			</td>

			<td class="byp_52">				

				<input type="number" name="byp_pixelate_data[<?php echo $grp_key; ?>][byp_2_4_v]" value="<?php echo $grp_val['byp_2_4_v']; ?>" class="byp_50 byp_max_50 large-text" /><span>%</span>				

			</td>

			<td class="byp_52">				

				<input type="number" name="byp_pixelate_data[<?php echo $grp_key; ?>][byp_2_2]" value="<?php echo $grp_val['byp_2_2']; ?>" class="byp_50 byp_max_50 large-text" />	<span>%</span>			

			</td>

			<td class="byp_52">				

				<input type="number" name="byp_pixelate_data[<?php echo $grp_key; ?>][byp_1_4_h]" value="<?php echo $grp_val['byp_1_4_h']; ?>" class="byp_50 byp_max_50 large-text" /><span>%</span>				

			</td>

			<td class="byp_52">				

				<input type="number" name="byp_pixelate_data[<?php echo $grp_key; ?>][byp_1_4_v]" value="<?php echo $grp_val['byp_1_4_v']; ?>" class="byp_50 byp_max_50 large-text" /><span>%</span>				

			</td>

			<td class="byp_52">				

				<input type="number" name="byp_pixelate_data[<?php echo $grp_key; ?>][byp_1_2_h]" value="<?php echo $grp_val['byp_1_2_h']; ?>" class="byp_50 byp_max_50 large-text" /><span>%</span>				

			</td>

			<td class="byp_52">				

				<input type="number" name="byp_pixelate_data[<?php echo $grp_key; ?>][byp_1_2_v]" value="<?php echo $grp_val['byp_1_2_v']; ?>" class="byp_50 byp_max_50 large-text" /><span>%</span>				

			</td>

			<td class="byp_52">

				<input type="number" name="byp_pixelate_data[<?php echo $grp_key; ?>][byp_1_1]" value="<?php echo $grp_val['byp_1_1']; ?>" class="byp_50 byp_max_50 large-text" />	<span>%</span>			

			</td>

			

			

			<td class="byp_52">	

				<span class="byp-action-btn byp-add-row" title="<?php _e('Add', 'buttons-with-style'); ?>"><i class="dashicons dashicons-plus-alt"></i></span>						

				<span class="byp-action-btn byp-del-row" title="<?php _e('Delete', 'buttons-with-style'); ?>"><i class="dashicons dashicons-trash"></i></span>

			</td>

		</tr>

		<?php

			} // End of foreach

		} // End if

		?>

		

	</table><!-- end .byp-group-button-sett -->



	<div class="btn-save-chnages">

		<input type="submit" id="bwswpos-settings-submit" name="bwswpos-settings-submit" class="button button-primary right" value="<?php _e('Save Changes','buttons-with-style');?>" />

	</div>

				

</form>



<form action="options.php" method="POST" id="byp-border-settings-form" class="byp-settings-form">

		

	<?php

	    settings_fields( 'byp_pix_border' );

	?>
	<div class="byp-sett-title byp-pixelate-title">Border Color</div>
	<table class="byp-border-table">	

		<tr>

			<th>R:</th>

			<th>G:</th>

			<th>B:</th>

		</tr>

		<tr>

			<td><input type="number" name="byp_border_data[byp_border_r]" class="" value="<?php echo $byp_border_r; ?>"></td>

			<td><input type="number" name="byp_border_data[byp_border_g]" class="" value="<?php echo $byp_border_g; ?>"></td>

			<td><input type="number" name="byp_border_data[byp_border_b]" class="" value="<?php echo $byp_border_b; ?>"></td>

		</tr>

	</table>

	<div class="btn-save-chnages">

		<input type="submit" id="byp-settings-submit" name="byp-settings-submit" class="button button-primary right" value="<?php _e('Save Changes','buttons-with-style');?>" />

	</div>

</form>





<script type="text/javascript">

	jQuery(document).ready(function(){	

		getColor();

	 

}); 



function rgbToHex(rgb) { 

		  var hex = Number(rgb).toString(16);

		  if (hex.length < 2) {

		       hex = "0" + hex;

		  }

		  return hex;

		};



	function rgbToHexcode(r, g, b) {

		var red = rgbToHex(r);

		  var green = rgbToHex(g);

		  var blue = rgbToHex(b);

	  return "#" + red+green+blue;

	}

function getColor(){

	jQuery(".byp_red, .byp_green, .byp_blue").keyup(function(){ 

	 	

	 	var key1 = jQuery(this).parents('tr').attr('data-key'); 

	 	 

	 	var red = jQuery("#"+key1+"_byp_row .byp_red").val();

	 	var green = jQuery("#"+key1+"_byp_row .byp_green").val();

	 	var blue = jQuery("#"+key1+"_byp_row .byp_blue").val();



	 	if('' != red && '' != green && '' != blue){

	 		

	 		var haxcode = rgbToHexcode(red, green, blue);

	 		

	 		jQuery("#"+key1+"_byp_row .byp_visual").val(haxcode);

	 		jQuery("#"+key1+"_byp_row .byp_visual_div").css("background-color",haxcode);

	 	}

	 });

}

</script>
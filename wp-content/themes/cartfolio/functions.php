<?php
/**
 * Set up the content width value based on the theme's design.
 *
 * @see tmpmela_content_width()
 *
 * @since TemplateMela 1.0
 */
if ( ! isset( $content_width ) ) {
	$content_width = 1400;
}

 


function tmpmela_setup() {
	/*
	* Makes Templatemela available for translation.
	*
	* Translations can be added to the /languages/ directory.
	* If you're building a theme based on tm, use a find and
	* replace to change 'cartfolio' to the name of your theme in all
	* template files.
	*/
	load_theme_textdomain( 'cartfolio', get_template_directory() . '/languages' );
	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, icons, and column width.
	 */
	add_editor_style( array( 'css/font-awesome.css', '/fonts/css/font-awesome.css', tmpmela_fonts_url() ) );
	// Adds RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );
	/*
	 * Switches default core markup for search form, comment form,
	 * and comments to output valid HTML5.
	 */
	add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );
	/*
	 * This theme supports all available post formats by default.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'audio', 'quote', 'link', 'gallery',
	) );
	global $wp_version;
	if ( version_compare( $wp_version, '3.4', '>=' ) ) {
		add_theme_support( 'custom-background' ); 
	}
	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		 'primary'   => esc_html__( 'TM Header Navigation', 'cartfolio' ),
		 'header-menu'   => esc_html__( 'TM Header Top Links', 'cartfolio' ),
		 'footer-menu'   => esc_html__( 'TM Footer Navigation', 'cartfolio' ),
	) );
	/*
	 * This theme uses a custom image size for featured images, displayed on
	 * "standard" posts and pages.
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 604, 270, true );
	// This theme uses its own gallery styles.
	add_filter( 'use_default_gallery_style', '__return_false' );
}
add_action( 'after_setup_theme', 'tmpmela_setup' );
/********************************************************
**************** TEMPLATE MELA CONTENT WIDTH ******************
********************************************************/
function tmpmela_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'tmpmela_content_width', 895 );
}
add_action( 'after_setup_theme', 'tmpmela_content_width', 0 );
/**
 * Getter function for Featured Content Plugin.
 *
 * @since TemplateMela 1.0
 *
 * @return array An array of WP_Post objects.
 */
function tmpmela_get_featured_posts() {
	/**
	 * Filter the featured posts to return in TemplateMela.
	 * @param array|bool $posts Array of featured posts, otherwise false.
	 */
	return apply_filters( 'tmpmela_get_featured_posts', array() );
}
/**
 * A helper conditional function that returns a boolean value.
 * @return bool Whether there are featured posts.
 */
function tmpmela_has_featured_posts() {
	return ! is_paged() && (bool) tmpmela_get_featured_posts();
}
/********************************************************
**************** TEMPLATE MELA SIDEBAR ******************
********************************************************/
function tmpmela_widgets_init() {
	register_sidebar( array(
		'name' => esc_html__( 'Main Sidebar', 'cartfolio' ),
		'id' => 'sidebar-1',
		'description' => esc_html__( 'Appears on posts and pages except the optional Front Page template, which has its own widgets', 'cartfolio' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );	
	register_sidebar( array(
		'name' => esc_html__( 'Footer copyright', 'cartfolio' ),
		'id' => 'foo_copyright',
		'description' => esc_html__( 'Appears on footer', 'cartfolio' ),
		'before_widget' => '',
		'after_widget' => "",
		'before_title' => '',
		'after_title' => '',
	) );	
}
add_action( 'widgets_init', 'tmpmela_widgets_init' );
/********************************************************
**************** TEMPLATE MELA FONT SETTING ******************
********************************************************/
function tmpmela_fonts_url() {
	$fonts_url = '';
	/* Translators: If there are characters in your language that are not
	 * supported by Source Sans Pro, translate this to 'off'. Do not translate
	 * into your own language.
	 */
	$source_sans_pro = _x( 'on', 'Source Sans Pro font: on or off', 'cartfolio' );
	/* Translators: If there are characters in your language that are not
	 * supported by Bitter, translate this to 'off'. Do not translate into your
	 * own language.
	 */
	$bitter = _x( 'on', 'Bitter font: on or off', 'cartfolio' );
	if ( 'off' !== $source_sans_pro || 'off' !== $bitter ) {
		$font_families = array();
		if ( 'off' !== $source_sans_pro )
			$font_families[] = 'Source Sans Pro:300,400,600,300italic,400italic,600italic';
		if ( 'off' !== $bitter )
			$font_families[] = 'Bitter:400,600';
		$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
			'subset' => urlencode( 'latin,latin-ext' ),
		);
		$fonts_url = esc_url( add_query_arg( $query_args, "//fonts.googleapis.com/css" ));
	}
	return $fonts_url;
}
/********************************************************
************ TEMPLATE MELA SCRIPT SETTING ***************
********************************************************/
function tmpmela_scripts_styles() {
	// Add Poppins fonts, used in the main stylesheet.
	wp_enqueue_style( 'tmpmela-fonts', tmpmela_fonts_url(), array(), null );
	// Add Genericons font, used in the main stylesheet.
	wp_enqueue_style( 'FontAwesome', get_template_directory_uri() . '/fonts/css/font-awesome.css', array(), '4.7.0' );
	// Loads our main stylesheet.
	wp_enqueue_style( 'tmpmela-style', get_stylesheet_uri(), array(), '1.0' );

	wp_enqueue_style( 'jquery-mCustomScrollbar-css', get_template_directory_uri() . '/css/jquery.mCustomScrollbar.css', array(), '4.7.0' );
	/*
	 * Adds JavaScript to pages with the comment form to support
	 * sites with threaded comments (when in use).
	 */
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'tmpmela-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20130402' );
	}
	// Loads JavaScript file with functionality specific to Templatemela.
	wp_enqueue_script( 'tmpmela-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '2014-02-01', true );
	// Adds JavaScript for handling the navigation menu hide-and-show behavior.
	wp_enqueue_script( 'tmpmela-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '1.0', true );

	/****************************************************************
	********** Script and style for IMAGE EDIT PAGE TEMPLATE ********
	*****************************************************************/
	
	/* custom style for image edit */
	wp_register_style('custom-img-edit', get_template_directory_uri().'/css/custom-img-edit.css', array(),rand(100,999));
	/* custom canvas image edit js */
	wp_register_script('cust-crop-img-js', get_template_directory_uri() . '/js/cust-crop-img.js', array( 'jquery' ), rand(100,999), true);
	wp_localize_script('cust-crop-img-js', 'cci', array(
            'ajaxurl' => admin_url('admin-ajax.php', ( is_ssl() ? 'https' : 'http')),
            
        ));
	/* crop css & js */
	wp_register_style('cropper-min-css', get_template_directory_uri().'/css/cropper-min.css', array(),rand(100,999));
	wp_register_script( 'cropper-min-js', get_template_directory_uri() . '/js/cropper.min.js', array( 'jquery' ),'1.0.0',true);

	 wp_enqueue_script( 'jquery-mCustomScrollbar-js', get_template_directory_uri() . '/js/jquery.mCustomScrollbar.js', array(), '1.0', true );
	/* img filters */


	/* check if needed*/
	wp_register_style('jquery-ui-css', get_template_directory_uri().'/css/jquery-ui.css', array(),'1.0.0');
	wp_register_script('jquery-ui-js', get_template_directory_uri() . '/js/jquery-ui.js', array(),'1.0.0',true);
	
	/* filter canvas*/
	wp_register_script('jquery-caman-js', get_template_directory_uri() . '/js/caman.full.min.js', array( 'jquery' ), '1.0.0', true );

	/* pixelet */
	wp_register_script('jquery-pixelate-js', get_template_directory_uri() . '/js/jquery.pixelate.js', array( 'jquery' ), rand(100,999), true);

	

	/*************************************************************************************
	***************** seperate js, css for bricks image on 22-5 approve *******************/

	/* custom style for image edit */
	wp_register_style('bricks-img-style', get_template_directory_uri().'/css/custom-bricks-img.css', array(),rand(100,999));
	/* custom canvas image edit js */
	wp_register_script('bricks-img-edit', get_template_directory_uri() . '/js/bricks-img-edit.js', array( 'jquery' ), rand(100,999), true);


	wp_register_style('all-css', get_template_directory_uri().'/css/all.css', array(),rand(100,999));
	wp_register_style('cropper-css', get_template_directory_uri().'/css/cropper.css', array(),rand(100,999));
	wp_register_style('main-css', get_template_directory_uri().'/css/main.css', array(),rand(100,999));
	wp_register_style('bootstrap-min-css', get_template_directory_uri().'/css/bootstrap.min.css', array(),rand(100,999));
	/* custom canvas image edit js */
	wp_register_script('jquery-slim-min-js', get_template_directory_uri() . '/js/jquery-3.3.1.slim.min.js', array( 'jquery' ), rand(100,999), true);
	wp_register_script('bootstrap-bundle-min-js', get_template_directory_uri() . '/js/bootstrap.bundle.min.js', array( 'jquery' ), rand(100,999), true);
	wp_register_script('cropper-js', get_template_directory_uri() . '/js/cropper.js', array( 'jquery' ), rand(100,999), true);
	wp_register_script('main-js', get_template_directory_uri() . '/js/main.js', array( 'jquery' ), rand(100,999), true);
	wp_register_script('bcs-js', get_template_directory_uri() . '/js/bcs.js', array( 'jquery' ), rand(100,999), true);
	

}
add_action( 'wp_enqueue_scripts', 'tmpmela_scripts_styles' );
/********************************************************
************ TEMPLATE MELA IMAGE ATTACHMENT ***************
********************************************************/
if ( ! function_exists( 'tmpmela_the_attached_image' ) ) :
/**
 * Print the attached image with a link to the next attached image.
 * @return void
 */
function tmpmela_the_attached_image() {
	/**
	 * Filter the image attachment size to use.
	 *
	 * @since Templatemela 1.0
	 *
	 * @param array $size {
	 *     @type int The attachment height in pixels.
	 *     @type int The attachment width in pixels.
	 * }
	 */
	$attachment_size     = apply_filters( 'tmpmela_attachment_size', array( 1400, 1100 ) );
	$next_attachment_url = wp_get_attachment_url();
	$post                = get_post();
	/*
	 * Grab the IDs of all the image attachments in a gallery so we can get the URL
	 * of the next adjacent image in a gallery, or the first image (if we're
	 * looking at the last image in a gallery), or, in a gallery of one, just the
	 * link to that image file.
	 */
	$attachment_ids = get_posts( array(
		'post_parent'    => $post->post_parent,
		'fields'         => 'ids',
		'numberposts'    => -1,
		'post_status'    => 'inherit',
		'post_type'      => 'attachment',
		'post_mime_type' => 'image',
		'order'          => 'ASC',
		'orderby'        => 'menu_order ID'
	) );
	// If there is more than 1 attachment in a gallery...
	if ( count( $attachment_ids ) > 1 ) {
		foreach ( $attachment_ids as $attachment_id ) {
			if ( $attachment_id == $post->ID ) {
				$next_id = current( $attachment_ids );
				break;
			}
		}
		// get the URL of the next image attachment...
		if ( $next_id )
			$next_attachment_url = get_attachment_link( $next_id );
		// or get the URL of the first image attachment.
		else
			$next_attachment_url = get_attachment_link( array_shift( $attachment_ids ) );
	}
	printf( '<a href="%1$s" title="%2$s" rel="attachment">%3$s</a>',
		esc_url( $next_attachment_url ),
		the_title_attribute( array( 'echo' => false ) ),
		wp_get_attachment_image( $post->ID, $attachment_size )
	);
}
endif;
/********************************************************
************ TEMPLATE MELA GET URL **********************
********************************************************/
function tmpmela_get_link_url() {
	$content = get_the_content();
	$has_url = get_url_in_content( $content );
	return ( $has_url ) ? $has_url : apply_filters( 'the_permalink', get_permalink() );
}
/********************************************************
************ TEMPLATE MELA LIST AUTHOR SETTING**************
********************************************************/
if ( ! function_exists( 'tmpmela_list_authors' ) ) :
/**
 * Print a list of all site contributors who published at least one post.
 * @return void
 */
function tmpmela_list_authors() {
	$contributor_ids = get_users( array(
		'fields'  => 'ID',
		'orderby' => 'post_count',
		'order'   => 'DESC',
		'who'     => 'authors',
	) );
	foreach ( $contributor_ids as $contributor_id ) :
		$post_count = count_user_posts( $contributor_id );
		// Move on if user has not published a post (yet).
		if ( ! $post_count ) {
			continue;
		}
	?>
<div class="contributor">
  <div class="contributor-info">
    <div class="contributor-avatar"><?php echo esc_attr(get_avatar( $contributor_id, 132 )); ?></div>
    <div class="contributor-summary">
      <h2 class="contributor-name"><?php echo esc_attr(get_the_author_meta( 'display_name', $contributor_id )); ?></h2>
      <p class="contributor-bio"> <?php echo esc_attr(get_the_author_meta( 'description', $contributor_id )); ?> </p>
      <a class="contributor-posts-link" href="<?php echo esc_url( get_author_posts_url( $contributor_id ) ); ?>"> <?php printf( _n( '%d Article', '%d Articles', $post_count, 'cartfolio' ), $post_count ); ?> </a> </div>
    <!-- .contributor-summary -->
  </div><!-- .contributor-info -->
</div><!-- .contributor -->
<?php
	endforeach;
}
endif;
/**
 * Extend the default WordPress body classes.
 *
 * Adds body classes to denote:
 * 1. Single or multiple authors.
 * 2. Presence of header image.
 * 3. Index views.
 * 4. Full-width content layout.
 * 5. Presence of footer widgets.
 * 6. Single views.
 * 7. Featured content layout.
 *
 * @since TemplateMela 1.0
 *
 * @param array $classes A list of existing body class values.
 * @return array The filtered body class list.
 */
function tmpmela_body_classes( $classes ) {
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}
	if ( get_header_image() ) {
		$classes[] = 'header-image';
	} else {
		$classes[] = 'masthead-fixed';
	}
	if ( is_archive() || is_search() || is_home() ) {
		$classes[] = 'list-view';
	}
	if ( ( ! is_active_sidebar( 'sidebar-2' ) )
		|| is_page_template( 'page-templates/full-width.php' )
		|| is_page_template( 'page-templates/contributors.php' )
		|| is_attachment() ) {
	}
	if ( is_singular() && ! is_front_page() ) {
		$classes[] = 'singular';
	}
	if ( is_front_page() && 'slider' == get_theme_mod( 'tmpmela_Featured_Content_layout' ) ) {
		$classes[] = 'slider';
	} elseif ( is_front_page() ) {
		$classes[] = 'grid';
	}
	return $classes;
}
add_filter( 'body_class', 'tmpmela_body_classes' );
/**
 * Extend the default WordPress post classes.
 *
 * Adds a post class to denote:
 * Non-password protected page with a post thumbnail.
 * @param array $classes A list of existing post class values.
 * @return array The filtered post class list.
 */
function tmpmela_post_classes( $classes ) {
	if ( ! post_password_required() && has_post_thumbnail() ) {
		$classes[] = 'has-post-thumbnail';
	}
	return $classes;
}
add_filter( 'post_class', 'tmpmela_post_classes' );
/**
 * Create a nicely formatted and more specific title element text for output
 * in head of document, based on current view.
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string The filtered title.
 */
function tmpmela_wp_title( $title, $sep ) {
	global $paged, $page;
	if ( is_feed() ) {
		return $title;
	}
	// Add the site name.
	$title .= get_bloginfo( 'name' );
	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title = "$title $sep $site_description";
	}
	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 ) {
		$title = "$title $sep " . sprintf( esc_html__( 'Page %s', 'cartfolio' ), max( $paged, $page ) );
	}
	return $title;
}
add_filter( 'wp_title', 'tmpmela_wp_title', 10, 2 );
// Implement Custom Header features.
require_once( trailingslashit( get_template_directory() ). 'inc/custom-header.php' );
// Custom template tags for this theme.
require_once( trailingslashit( get_template_directory() ). 'inc/template-tags.php' );
// Add Theme Customizer functionality.
require_once( trailingslashit( get_template_directory() ). 'inc/customizer.php' );
/*
 * Add Featured Content functionality.
 *
 * To overwrite in a plugin, define your own tmpmela_Featured_Content class on or
 * before the 'setup_theme' hook.
*/
if ( ! class_exists( 'tmpmela_Featured_Content' ) && 'plugins.php' !== $GLOBALS['pagenow'] ) {	
	require_once( trailingslashit( get_template_directory() ). 'inc/featured-content.php' );
}
function tmpmela_title_tag() {
   add_theme_support( 'title-tag' );
}
add_action( 'after_setup_theme', 'tmpmela_title_tag' );
/*Add Templatemela custom function */
require_once( trailingslashit( get_template_directory() ). 'templatemela/megnor-functions.php' );
/*Add Templatemela theme setting in menu */
require_once( trailingslashit( get_template_directory() ). 'templatemela/options.php' );
/*Add TGMPA library file */
require  trailingslashit(get_template_directory()).'/templatemela/tmpmela-plugins-install.php' ;
add_action( 'admin_menu', 'tmpmela_theme_setting_menu' );
function tmpmela_theme_settings_page() {
	$locale_file = get_template_part('templatemela/admin/theme-setting');
	if (is_readable( $locale_file ))		
		require_once( trailingslashit( get_template_directory() ). 'templatemela/admin/theme-setting.php' );
}
function tmpmela_hook_manage_page() {
	$locale_file = get_template_part('templatemela/admin/theme-hook') ;
	if (is_readable( $locale_file ))		
		require_once( trailingslashit( get_template_directory() ). 'templatemela/admin/theme-hook.php' );
}
/* Control Panel Tags Function Includes */
require_once( trailingslashit( get_template_directory() ). 'templatemela/controlpanel/tmpmela_control_panel.php' );
require_once( trailingslashit( get_template_directory() ). 'templatemela/admin/hook-functions.php' );
require_once( trailingslashit( get_template_directory() ). 'mr-image-resize.php' );
/* Adds woocommerce functions if active */
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) :
	require_once( trailingslashit( get_template_directory() ). 'templatemela/woocommerce-functions.php' );
endif;
/*for single post related page */
if ( ! function_exists( 'tmpmela_related_posts' ) ) :
function tmpmela_related_posts() {
global $post;
	$orig_post = $post;
$categories = get_the_category($post->ID);
if ($categories) {
$category_ids = array();
foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
$args=array(
'category__in' => $category_ids,
'post__not_in' => array($post->ID),
'posts_per_page'=> 2, // Number of related posts that will be shown.
'ignore_sticky_posts'=>1
);
$my_query = new wp_query( $args );
if( $my_query->have_posts() ) {   ?>
<div class="related_posts blog-list"><h3 class="big-title"> <?php echo esc_html_e('Related Posts','cartfolio');  ?></h3>
<?php while( $my_query->have_posts() ) {
$my_query->the_post();?>
<article class="post">
<div class="entry-main-content">
<div class="entry-thumbnail">
	<a href="<?php the_permalink()?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail(); ?></a>
</div>	
<div class="post-content">
	<div class="entry-main-header ">			
		<header class="entry-header"><h3 class="entry-title"> 
		<a href="<?php the_permalink()?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title_attribute(); ?></a>
		</h3></header>
	</div>
	<div class="entry-content-inner">
				<div class="entry-meta-inner">
				  <div class="entry-meta">
					<?php tmpmela_author_link(); ?>
					<?php tmpmela_entry_date(); ?>					
				 </div> <!--  entry-meta -->
			</div><!--  entry-meta-inner -->
	 </div>
</div>	
</div>	
</article>		
<?php
}
echo '</div>';
}
}
$post = $orig_post;
wp_reset_postdata(); 
}
endif;
/********************************************************
**************** One Click Import Data ******************
********************************************************/

if ( ! function_exists( 'sampledata_import_files' ) ) :
function sampledata_import_files() {
    return array(
		 array(
            'import_file_name'             => 'cartfolio',
            'local_import_file'            => trailingslashit( get_stylesheet_directory() ) . 'demo-content/default/cartfolio.wordpress.xml',
            'local_import_customizer_file' => trailingslashit( get_stylesheet_directory() ) . 'demo-content/default/cartfolio_customizer_export.dat',
			'local_import_widget_file'     => trailingslashit( get_stylesheet_directory() ) . 'demo-content/default/cartfolio_widgets_settings.wie',
            'import_notice'                => esc_html__( 'Please waiting for a few minutes, do not close the window or refresh the page until the data is imported.', 'cartfolio' ),
        ),
		);
}
add_filter( 'pt-ocdi/import_files', 'sampledata_import_files' );
endif;

if ( ! function_exists( 'sampledata_after_import' ) ) :
function sampledata_after_import($selected_import) {
         //Set Menu
        $header_menu = get_term_by('name', 'MainMenu', 'nav_menu');
        $top_menu = get_term_by('name', 'Header Top Links', 'nav_menu');
		$footer_menu = get_term_by('name', 'MainMenu', 'nav_menu');
        set_theme_mod( 'nav_menu_locations' , array( 
		 'primary'   => $header_menu->term_id,
		 'header-menu'   => $top_menu->term_id ,
		 'footer-menu'   => $footer_menu->term_id 
         ) 
        );
		
		//Set Front page and blog page
       $page = get_page_by_title( 'Home');
       if ( isset( $page->ID ) ) {
        update_option( 'page_on_front', $page->ID );
        update_option( 'show_on_front', 'page' );
       }
	   $post = get_page_by_title( 'Blog');
       if ( isset( $page->ID ) ) {
        update_option( 'page_for_posts', $post->ID );
        update_option( 'show_on_posts', 'post' );
       }
	   
	   //Import Revolution Slider
       if ( class_exists( 'RevSlider' ) ) {
           $slider_array = array(
              get_stylesheet_directory()."/demo-content/default/tmpmela_homeslider.zip",
			  get_stylesheet_directory()."/demo-content/default/tmpmela_homeslider_video.zip", 
              );
           $slider = new RevSlider();
        
           foreach($slider_array as $filepath){
             $slider->importSliderFromPost(true,true,$filepath);  
           }
           echo esc_html__( 'Slider processed', 'cartfolio' );
      }
}
add_action( 'pt-ocdi/after_import', 'sampledata_after_import' );
endif;



/* add metabox*/
add_action( 'add_meta_boxes', 'bwsp_post_sett_metabox' );
function bwsp_post_sett_metabox() {
	add_meta_box( 'byp-post-sett', __( 'Mosaic Data', 'buttons-with-style' ),'bwsp_post_sett_mb_content', 'product', 'side', 'high' );
	}


function bwsp_post_sett_mb_content(){

	global $post;
	$post_id = $post->ID; 	
	?>

 	<div class="csv-download" style="padding: 10px; margin-top: 10px; margin-left: 66px;">	
 		<a href="<?php echo admin_url( 'admin-post.php' ) ?>?action=print.csv&_wpnonce=<?php echo $post_id?>" class="page-title-action"><?php _e('Export to CSV','my-plugin-slug');?></a>	
	</div>	
<?php }

if ( isset($_GET['action'] ) && $_GET['action'] == 'print.csv' )  {
	// Handle CSV Export
	add_action( 'admin_post_print.csv', 'print_csv' );
}

function print_csv()
{
     $nonce = isset( $_GET['_wpnonce'] ) ? $_GET['_wpnonce'] : '';
	
 	header('Content-Type: application/csv');
    header('Content-Disposition: attachment; filename=example.csv');
    header('Pragma: no-cache');
    
    $fh = @fopen( 'php://output', 'w' );
    fprintf( $fh, chr(0xEF) . chr(0xBB) . chr(0xBF) );

     $byp_csv_data = get_post_meta($nonce, 'byp_csv_data')[0];

	$myArray = explode(',', $byp_csv_data);

	$csv_array = array();
	foreach ($myArray as $value) {
		$csv_array[] = $value;
	}

   	$byp_chunk_arr = array_chunk($csv_array,7);

	foreach ($byp_chunk_arr as $value) {
		 fputcsv( $fh, $value );
	}

     fclose( $fh );
    // output the CSV data
}


function tmpmela_change_time_of_single_ajax_call() {
	return 10;
}
add_action( 'pt-ocdi/time_for_one_ajax_call', 'tmpmela_change_time_of_single_ajax_call' );
/* remove notice info*/
add_filter( 'pt-ocdi/disable_pt_branding', '__return_true' ); 

// Ajax call to update calculations
add_action('wp_ajax_byp_get_pixelate_optns', 'byp_get_pixelate_optns');
add_action('wp_ajax_nopriv_byp_get_pixelate_optns', 'byp_get_pixelate_optns');

 function byp_get_pixelate_optns(){

	$filter_clr = $_POST['filter_clr'];
	$byp_pixelate_data 			= get_option('byp_pixelate_data') ? get_option('byp_pixelate_data') : array('0' => '');
	$byp_pixelate_data			= !empty($byp_pixelate_data) ? $byp_pixelate_data : array('0' => '');
	$byp_border_data = get_option('byp_border_data');

	$byp_border_r 			= $byp_border_data['byp_border_r'] ? $byp_border_data['byp_border_r'] : '';
	$byp_border_g 			= $byp_border_data['byp_border_g'] ? $byp_border_data['byp_border_g'] : '';
	$byp_border_b 			= $byp_border_data['byp_border_b'] ? $byp_border_data['byp_border_b'] : '';
	$pixelate_properties = array();
	$pix_border_prop = array();

	if(!empty($byp_border_r) && !empty($byp_border_g) && !empty($byp_border_b)){
		$pix_border_prop = array('r' => $byp_border_r,
								'g' => 	$byp_border_g,
								'b' => $byp_border_b,
								);
	}

	if(!empty($byp_pixelate_data)) { 
		foreach ($byp_pixelate_data as $grp_key => $grp_val) {

			$byp_clr_name = isset($grp_val['byp_clr_name']) 	? $grp_val['byp_clr_name'] 		: '';
			$byp_red = isset($grp_val['byp_red']) 	? $grp_val['byp_red'] 		: '';
			$byp_green = isset($grp_val['byp_green']) 	? $grp_val['byp_green'] 		: '';
			$byp_blue = isset($grp_val['byp_blue']) 	? $grp_val['byp_blue'] 		: '';
			
			$byp_visual = isset($grp_val['byp_visual']) 	? $grp_val['byp_visual'] 		: '';
			$byp_visual = ($byp_visual == 'byp_visual') 	? 'yes' : 'no';
			$byp_fullclr = isset($grp_val['byp_fullclr']) 	? $grp_val['byp_fullclr'] 		: '';
			$byp_fullclr = ($byp_fullclr == 'byp_fullclr') 	? 'yes'	: 'no';
			$byp_grayscale = isset($grp_val['byp_grayscale']) 	? $grp_val['byp_grayscale'] : '';
			$byp_grayscale = ($byp_grayscale == 'byp_grayscale') 	? 'yes' : 'no';
			
			$byp_stock = isset($grp_val['byp_stock']) 	? $grp_val['byp_stock'] : '';
			$byp_stock = ($byp_stock == 'byp_stock') 	? 'yes' : 'no';
			$byp_2_8_h = isset($grp_val['byp_2_8_h']) 	? $grp_val['byp_2_8_h'] : '';
			$byp_2_8_v = isset($grp_val['byp_2_8_v']) 	? $grp_val['byp_2_8_v'] : '';
			$byp_2_6_h = isset($grp_val['byp_2_6_h']) 	? $grp_val['byp_2_6_h'] : '';
			$byp_2_6_v = isset($grp_val['byp_2_6_v']) 	? $grp_val['byp_2_6_v'] : '';
			$byp_1_8_h = isset($grp_val['byp_1_8_h']) 	? $grp_val['byp_1_8_h'] : '';
			$byp_1_8_v = isset($grp_val['byp_1_8_v']) 	? $grp_val['byp_1_8_v'] : '';
			$byp_1_6_h = isset($grp_val['byp_1_6_h']) 	? $grp_val['byp_1_6_h'] : '';
			$byp_1_6_v = isset($grp_val['byp_1_6_v']) 	? $grp_val['byp_1_6_v'] : '';
			$byp_2_4_h = isset($grp_val['byp_2_4_h']) 	? $grp_val['byp_2_4_h'] : '';
			$byp_2_4_v = isset($grp_val['byp_2_4_v']) 	? $grp_val['byp_2_4_v'] : '';
			$byp_2_2 = isset($grp_val['byp_2_2']) 	? $grp_val['byp_2_2'] : '';
			$byp_1_4_h = isset($grp_val['byp_1_4_h']) 	? $grp_val['byp_1_4_h'] : '';
			$byp_1_4_v = isset($grp_val['byp_1_4_v']) 	? $grp_val['byp_1_4_v'] : '';
			$byp_1_2_h = isset($grp_val['byp_1_2_h']) 	? $grp_val['byp_1_2_h'] : '';
			$byp_1_2_v = isset($grp_val['byp_1_2_v']) 	? $grp_val['byp_1_2_v'] : '';
			$byp_1_1 = isset($grp_val['byp_1_1']) 	? $grp_val['byp_1_1'] : '';
			
			if($filter_clr == 'fullcolor'){
				if($byp_fullclr == 'yes'){
					$pixelate_properties[]	= array( "r" => $byp_red,
		    										"g" => $byp_green,
		    										"b" =>  $byp_blue,
		    										"color" =>  $byp_clr_name,
		    										"full_color" => $byp_fullclr,
													"gray_scale" => $byp_grayscale,
													"in_stock" => $byp_stock,
													"1" => $byp_2_8_h,
													"2" => $byp_2_8_v,
													"3" => $byp_2_6_h,
													"4" => $byp_2_6_v,
													"5" => $byp_1_8_h,
													"6" => $byp_1_8_v,
													"7" => $byp_1_6_h,
													"8" => $byp_1_6_v,
													"9" => $byp_2_4_h,
													"10" => $byp_2_4_v,
													"11" => $byp_2_2,
													"12" => $byp_1_4_h,
													"13" => $byp_1_4_v,
													"14" => $byp_1_2_h,
													"15" => $byp_1_2_v,
													"16" => $byp_1_1,
	    										);   
				}
			} else if($filter_clr == 'gray'){
				if($byp_fullclr == 'yes'){
					$pixelate_properties[]	= array( "r" => $byp_red,
		    										"g" => $byp_green,
		    										"b" =>  $byp_blue,
		    										"color" =>  $byp_clr_name,
		    										"full_color" => $byp_fullclr,
													"gray_scale" => $byp_grayscale,
													"in_stock" => $byp_stock,
													"1" => $byp_2_8_h,
													"2" => $byp_2_8_v,
													"3" => $byp_2_6_h,
													"4" => $byp_2_6_v,
													"5" => $byp_1_8_h,
													"6" => $byp_1_8_v,
													"7" => $byp_1_6_h,
													"8" => $byp_1_6_v,
													"9" => $byp_2_4_h,
													"10" => $byp_2_4_v,
													"11" => $byp_2_2,
													"12" => $byp_1_4_h,
													"13" => $byp_1_4_v,
													"14" => $byp_1_2_h,
													"15" => $byp_1_2_v,
													"16" => $byp_1_1,
	    										);      
				}
			}

		}
	}
	$all_prop = array();
	$all_prop = array('pixelate_properties' => $pixelate_properties, 'pix_border_prop' => $pix_border_prop);
	$all_prop = json_encode($all_prop);
	
	//$all_prop = preg_replace('/"([^"]+)"\s*:\s*/', '$1:', $all_prop);
	
	print_r($all_prop);

	exit();
}

// Ajax call to update calculations
add_action('wp_ajax_byp_add_order', 'byp_add_order');
add_action('wp_ajax_nopriv_byp_add_order', 'byp_add_order');

function byp_add_order(){

   global $wp_query;    
  
   /* insert mosaic image*/
   $mosaic_img 			= $_POST['mosaic_img'];   
   $minpix 				= $_POST['minpix'];   
   $maxpix 				= $_POST['maxpix'];  
   $m_image_name 		= $_POST['img_name'];
   $product_id 			= $_POST['product_id'];
   $byp_csv_data 		= $_POST['byp_csv_data'];


   $m_image_name = ($product_id != '') ? get_field("image_name",$product_id) : $_POST['img_name'];
   /* check if name has extension*/
	$supported_image = array( 'gif', 'jpg', 'jpeg', 'png' );

	$ext = strtolower(pathinfo($m_image_name, PATHINFO_EXTENSION)); // Using strtolower to overcome case sensitive
	if (in_array($ext, $supported_image)) {
	    $m_image_name = $m_image_name;
	} else {
	    $m_image_name = $m_image_name.".jpg";
	}

    $mosaic_img_name 	= 'mozaïek-'.$m_image_name;
   

   $mo_image_data      	= file_get_contents($mosaic_img); // Get image data
   $mo_wp_upload_dir  	= wp_upload_dir(); 
   $mo_unique_file_name = wp_unique_filename( $mo_wp_upload_dir['path'], $mosaic_img_name ); 
   $mo_filename         = basename( $mo_unique_file_name ); 

   if( wp_mkdir_p( $mo_wp_upload_dir['path'] ) ) {
	    $mo_file 		= $mo_wp_upload_dir['path'] . '/' . $mo_filename;
	} else {
	    $mo_file 		= $mo_wp_upload_dir['basedir'] . '/' . $mo_filename;
	}
	// Create the image  file on the server
	file_put_contents( $mo_file, $mo_image_data );

	// Check image file type
	$mo_wp_filetype 	= wp_check_filetype( $mo_filename, null );

	// Set attachment data
	$mo_attachment 		= array(
						    'post_mime_type' => $mo_wp_filetype['type'],
						    'post_title'     => sanitize_file_name( $mo_filename ),
						    'post_content'   => '',
						    'post_status'    => 'inherit'
						);

	// Create the attachment
	$mo_attach_id 		= wp_insert_attachment( $mo_attachment, $mo_file );
	$mo_attached_url 	= wp_get_attachment_url($mo_attach_id);
	// Include image.php
	require_once(ABSPATH . 'wp-admin/includes/image.php');
	
	// Define attachment metadata
	$mo_attach_data = wp_generate_attachment_metadata( $mo_attach_id, $mo_file );
	
	// Assign metadata to attachment
	wp_update_attachment_metadata( $mo_attach_id, $mo_attach_data );
	
   /******************************** end insert mosaic image **********************************/
   /******************************** insert cropped image **********************************/
  
   $cropped_img 		= $_POST['cropped_img'];
   $cropped_img_name 	= 'cropped-'.$m_image_name;

   $crp_image_data      = file_get_contents($cropped_img); // Get image data
   $crp_wp_upload_dir  	= wp_upload_dir(); 
   $crp_unique_file_name = wp_unique_filename( $crp_wp_upload_dir['path'], $cropped_img_name ); // Gen unique name
   $crp_filename         = basename( $crp_unique_file_name ); 

   if( wp_mkdir_p( $crp_wp_upload_dir['path'] ) ) {
	    $crp_file 		= $crp_wp_upload_dir['path'] . '/' . $crp_filename;
	} else {
	    $crp_file 		= $crp_wp_upload_dir['basedir'] . '/' . $crp_filename;
	}
	// Create the image  file on the server
	file_put_contents( $crp_file, $crp_image_data );

	// Check image file type
	$crp_wp_filetype 	= wp_check_filetype( $crp_filename, null );	
	// Set attachment data
	$crp_attachment 		= array(
						    'post_mime_type' => $crp_wp_filetype['type'],
						    'post_title'     => sanitize_file_name( $crp_filename ),
						    'post_content'   => '',
						    'post_status'    => 'inherit'
						);

	// Create the attachment
	$crp_attach_id 		= wp_insert_attachment( $crp_attachment, $crp_file );
	$crp_attached_url 	= wp_get_attachment_url($crp_attach_id);
	// Include image.php
	require_once(ABSPATH . 'wp-admin/includes/image.php');

	// Define attachment metadata
	$crp_attach_data = wp_generate_attachment_metadata( $crp_attach_id, $crp_file );
	
	// Assign metadata to attachment
	wp_update_attachment_metadata( $crp_attach_id, $crp_attach_data );

   /******************************** end insert cropped image **********************************/
   /******************************** insert original image **********************************/

   $original_img 		= $_POST['original_img'];
   $original_img_name 	= 'original-'.$m_image_name;

   $ori_image_data      	= file_get_contents($original_img); // Get image data
   $ori_wp_upload_dir  	= wp_upload_dir(); 
   $ori_unique_file_name = wp_unique_filename( $ori_wp_upload_dir['path'], $original_img_name ); // Gen unique name
   $ori_filename         = basename( $ori_unique_file_name ); 

   if( wp_mkdir_p( $ori_wp_upload_dir['path'] ) ) {
	    $ori_file 		= $ori_wp_upload_dir['path'] . '/' . $ori_filename;
	} else {
	    $ori_file 		= $ori_wp_upload_dir['basedir'] . '/' . $ori_filename;
	}
	// Create the image  file on the server
	file_put_contents( $ori_file, $ori_image_data );

	// Check image file type
	$ori_wp_filetype 	= wp_check_filetype( $ori_filename, null );

	// Set attachment data
	$ori_attachment 		= array(
						    'post_mime_type' => $ori_wp_filetype['type'],
						    'post_title'     => sanitize_file_name( $ori_filename ),
						    'post_content'   => '',
						    'post_status'    => 'inherit'
						);

	// Create the attachment
	$ori_attach_id 		= wp_insert_attachment( $ori_attachment, $ori_file );
	$ori_attached_url 	= wp_get_attachment_url($ori_attach_id);
	// Include image.php
	require_once(ABSPATH . 'wp-admin/includes/image.php');

	// Define attachment metadata
	$ori_attach_data = wp_generate_attachment_metadata( $ori_attach_id, $ori_file );
	
	// Assign metadata to attachment
	wp_update_attachment_metadata( $ori_attach_id, $ori_attach_data );

   /******************************** end insert original image **********************************/
   	$product_name = substr($mo_filename, 0, strpos($mo_filename, "."));
   	$product_name = $product_name.'-'.$minpix.'x'.$maxpix;
   	$pro_img_price 	= $_POST['img_price'];

	/* create woo product with this img */
	if($product_id){

		$mo_post = array(
		'ID'           => $product_id,
	    'post_author' => get_current_user_id(),
	    'post_content' => '',
	    'post_status' => "publish",
	    'post_title' => "mozaïek-".$minpix."x".$maxpix,
	    'post_parent' => '',
	    'post_type' => "product",
		);

		wp_update_post($mo_post, $wp_error);
     	$mo_post_id = $product_id;
     	update_post_meta($mo_post_id, 'byp_csv_data', $byp_csv_data);


	} else {
		
		$mo_post = array(
	    'post_author' => get_current_user_id(),
	    'post_content' => '',
	    'post_status' => "publish",
	    'post_title' => "mozaïek-".$minpix."x".$maxpix,
	    'post_parent' => '',
	    'post_type' => "product",
		);
		/*'post_title' => sanitize_file_name( $product_name ),*/

		$mo_post_id = wp_insert_post( $mo_post, $wp_error );

		add_post_meta($mo_post_id, 'byp_csv_data', $byp_csv_data);
	}
	

	set_post_thumbnail( $mo_post_id, $mo_attach_id );
	 
	/* add image into acf fields */
	update_field( 'image_name', $m_image_name, $mo_post_id );
	update_field( 'mosaic_image', $mo_attached_url, $mo_post_id );
	update_field( 'cropped_image', $crp_attached_url, $mo_post_id );
	update_field( 'original_image', $ori_attached_url, $mo_post_id );
	update_field( 'pix_image_width', $minpix, $mo_post_id );
	update_field( 'pix_image_height', $maxpix, $mo_post_id );
	$minpix.'x'.$maxpix;

	wp_set_object_terms( $mo_post_id, 'Races', 'product_cat' );
	wp_set_object_terms($mo_post_id, 'simple', 'product_type');

	update_post_meta( $mo_post_id, '_visibility', 'visible' );
	update_post_meta( $mo_post_id, '_stock_status', 'instock');
	update_post_meta( $mo_post_id, 'total_sales', '0');
	update_post_meta( $mo_post_id, '_downloadable', 'yes');
	update_post_meta( $mo_post_id, '_virtual', 'yes');
	update_post_meta( $mo_post_id, '_regular_price', $pro_img_price );
	/*update_post_meta( $mo_post_id, '_sale_price', '00' );*/
	update_post_meta( $mo_post_id, '_purchase_note', "" );
	update_post_meta( $mo_post_id, '_featured', "no" );
	update_post_meta( $mo_post_id, '_weight', "" );
	update_post_meta( $mo_post_id, '_length', "" );
	update_post_meta( $mo_post_id, '_width', "" );
	update_post_meta( $mo_post_id, '_height', "" );
	update_post_meta($mo_post_id, '_sku', "");
	update_post_meta( $mo_post_id, '_product_attributes', array());
	update_post_meta( $mo_post_id, '_sale_price_dates_from', "" );
	update_post_meta( $mo_post_id, '_sale_price_dates_to', "" );
	update_post_meta( $mo_post_id, '_price', $pro_img_price );
	update_post_meta( $mo_post_id, '_sold_individually', "" );
	update_post_meta( $mo_post_id, '_manage_stock', "no" );
	update_post_meta( $mo_post_id, '_backorders', "no" );
	update_post_meta( $mo_post_id, '_stock', "" ); 	
	echo  $mo_post_id;
	
	exit(); 
}

/* change text and url to empty cart page*/
add_filter( 'gettext', 'change_woocommerce_return_to_shop_text', 20, 3 );
function change_woocommerce_return_to_shop_text( $translated_text, $text, $domain ) {
    switch ( $translated_text ) {
   		case 'Return to shop' :
   		$translated_text = __( 'Return to Build your picture', 'woocommerce' );
   		break;
  	}
 	return $translated_text;
}

/**
 * Changes the redirect URL for the Return To Shop button in the cart.
 */
function wc_empty_cart_redirect_url() {
	$aandeslagpg = get_permalink( 8297 );
	return $aandeslagpg;
}
add_filter( 'woocommerce_return_to_shop_redirect', 'wc_empty_cart_redirect_url' );

//Changed checkout to thank you title in order received page
function byp_title_order_received( $title, $id ) {
	if ( is_order_received_page() && get_the_ID() === $id ) {
		$title = "Thank you!";
	}
	return $title;
}
add_filter( 'the_title', 'byp_title_order_received', 10, 2 );

/**
* order detail page customisation 
**/

add_action( 'woocommerce_before_order_itemmeta', 'storage_location_of_order_items', 10, 3 );
function storage_location_of_order_items( $item_id, $item, $product ){
    // Only on backend order edit pages
    if( ! ( is_admin() && $item->is_type('line_item') ) ) return;

    // Get your ACF product value (replace the slug by yours below)
    if( $acf_value = get_field( 'mosaic_image', $product->get_id() ) ) {
        $acf_label = __('Mosaic Image: ');
        $img_width = get_field('pix_image_width', $_product->id);
		$img_height = get_field('pix_image_height', $_product->id);	
		if($img_width){
			$pro_name = 'mozaïek-'.$img_width.'x'.$img_height;
		} else{
			$pro_name = 'mozaïek';
		} ?>						
        
        <span class="byp_pro_name"><?php echo $pro_name; ?></span>
        <span class="wc-order-item-thumbnail byp-list-img"><img width="150" height="150" src="<?php echo $acf_value ?>" class="attachment-thumbnail size-thumbnail" alt="" title=""></span>
        <a href="<?php echo $acf_value ?>" class="byp-img-dwnld" download>download</a>

    <?php }

    if( $acf_cropped_image = get_field( 'cropped_image', $product->get_id() ) ) {
        $acf_label = __('Cropped Image: ');

        $img_width = get_field('pix_image_width', $_product->id);
		$img_height = get_field('pix_image_height', $_product->id);	
		if($img_width){
			$pro_name = 'cropped-'.$img_width.'x'.$img_height;
		} else{
			$pro_name = 'cropped';
		} ?>
        <span class="byp_pro_name"><?php echo $pro_name; ?></span> 
        
        <span class="wc-order-item-thumbnail byp-list-img"><img width="150" height="150" src="<?php echo $acf_cropped_image ?>" class="attachment-thumbnail size-thumbnail" alt="" title=""></span>
        <a href="<?php echo $acf_value ?>" class="byp-img-dwnld" download>download</a>

    <?php }
    if( $acf_original_image = get_field( 'original_image', $product->get_id() ) ) {
        $acf_label_edited = __('Original Image: ');
        $img_width = get_field('pix_image_width', $_product->id);
		$img_height = get_field('pix_image_height', $_product->id);	
		if($img_width){
			$pro_name = 'original-'.$img_width.'x'.$img_height;
		} else{
			$pro_name = 'original';
		}						
         ?>
        <span class="byp_pro_name"><?php echo $pro_name; ?></span> 
        <span class="wc-order-item-thumbnail byp-list-img "><img width="150" height="150" src="<?php echo $acf_original_image ?>" class="attachment-thumbnail size-thumbnail" alt="" title=""></span><a href="<?php echo $acf_value ?>" class="byp-img-dwnld last" download>download</a>
    <?php }
}

//enque admin script
function wpdocs_selectively_enqueue_admin_script( $hook ) {
   
   wp_enqueue_script( 'byp_admin_script', get_template_directory_uri() . '/js/byp-admin.js', array('wp-color-picker'), rand(100,999) );
    wp_enqueue_style('byp_admin_style', get_template_directory_uri() . '/css/byp-admin.css', array(), rand(100,999) );

    wp_enqueue_script('wp-color-picker');
    wp_enqueue_style('wp-color-picker');
}
add_action( 'admin_enqueue_scripts', 'wpdocs_selectively_enqueue_admin_script' );

/**
 * Adds product images to the WooCommerce order emails table
 * Uses WooCommerce 2.5 or newer
 *
 * @param string $output the buffered email order items content
 * @param \WC_Order $order
 * @return $output the updated output
 */
function sww_add_images_woocommerce_emails( $output, $order ) {
	
	// set a flag so we don't recursively call this filter
	static $run = 0;
  
	// if we've already run this filter, bail out
	if ( $run ) {
		return $output;
	}
  
	$args = array(
		'show_image'   	=> true,
		'image_size'    => array( 100, 100 ),
	);
  
	// increment our flag so we don't run again
	$run++;
  
	// if first run, give WooComm our updated table
	return $order->email_order_items_table( $args );
}
add_filter( 'woocommerce_email_order_items_table', 'sww_add_images_woocommerce_emails', 10, 2 );

function register_load_fragments_script() {
    // Register the script
    wp_register_script( 'load-fragments-script', get_template_directory_uri() . '/js/load-fragments.js', array(), '20170121', true  );

    // Substitustions in script
    $translation_array = array(
        'cart_hash_key' => WC()->ajax_url() . '-wc_cart_hash'
    );
    wp_localize_script( 'load-fragments-script', 'substitutions', $translation_array );

    wp_enqueue_script( 'load-fragments-script' );
}

add_action( 'woocommerce_widget_shopping_cart_buttons', function(){
    // Removing Buttons
    remove_action( 'woocommerce_widget_shopping_cart_buttons', 'woocommerce_widget_shopping_cart_button_view_cart', 10 );
    remove_action( 'woocommerce_widget_shopping_cart_buttons', 'woocommerce_widget_shopping_cart_proceed_to_checkout', 20 );

    // Adding customized Buttons
    add_action( 'woocommerce_widget_shopping_cart_buttons', 'custom_widget_shopping_cart_button_view_cart', 10 );
    add_action( 'woocommerce_widget_shopping_cart_buttons', 'custom_widget_shopping_cart_proceed_to_checkout', 20 );
}, 1 );

// Custom cart button
function custom_widget_shopping_cart_button_view_cart() {
    $original_link = wc_get_cart_url();
    echo '<a href="' . esc_url( $original_link ) . '" class="button wc-forward">' . esc_html__( 'Naar je winkelmand', 'woocommerce' ) . '</a>';
}

// Custom Checkout button
function custom_widget_shopping_cart_proceed_to_checkout() {
    $original_link = wc_get_checkout_url();
    echo '<a href="' . esc_url( $original_link ) . '" class="button checkout wc-forward">' . esc_html__( 'Afrekenen', 'woocommerce' ) . '</a>';
}

//replace includes tax text
add_filter( 'gettext', function( $translation, $text, $domain ) {
	if ( $domain == 'woocommerce' ) {
		if ( $text == '(includes %s)' ) { $translation = '(inclusief %s)'; }
	}
	return $translation;
}, 10, 3 );



add_filter('gettext', 'wpse_124400_woomessages', 10, 3);

/**
* change some WooCommerce labels
* @param string $translation
* @param string $text
* @param string $domain
* @return string
*/
function wpse_124400_woomessages($translation, $text, $domain) {
    if ($domain == 'woocommerce') {
        if ($text == 'Cart updated.') {
            $translation = 'winkelmand is bijgewerkt.';
        }
    }

    return $translation;
}

/**
 * @snippet       Add Content to Empty Cart Page - WooCommerce
 * @how-to        Watch tutorial @ https://businessbloomer.com/?p=19055
 * @sourcecode    https://businessbloomer.com/?p=21585
 * @author        Rodolfo Melogli
 * @compatible    WC 2.6.14, WP 4.7.2, PHP 5.5.9
 */
 
add_action( 'woocommerce_cart_is_empty', 'bbloomer_add_content_empty_cart' );
 
function bbloomer_add_content_empty_cart() {
echo '<p class="cart-empty byp-show-msg">huh? je winkelmand is nog leeg!</p>';
}

// rename the "Have a Coupon?" message on the checkout page
function woocommerce_rename_coupon_message_on_checkout() {
	return 'Heb je een voucher code?' . ' <a href="#" class="showcoupon">' . __( ' Vul \'m hier in', 'woocommerce' ) . '</a>';
}
add_filter( 'woocommerce_checkout_coupon_message', 'woocommerce_rename_coupon_message_on_checkout' );

/**
 * @snippet       Translate a String in WooCommerce
 * @how-to        Watch tutorial @ https://businessbloomer.com/?p=19055
 * @sourcecode    https://businessbloomer.com/?p=162
 * @author        Rodolfo Melogli
 * @compatible    WooCommerce 3.5.4
 * @donate $9     https://businessbloomer.com/bloomer-armada/
 */
 
add_filter( 'gettext', 'bbloomer_translate_woocommerce_strings', 999, 3 );
 
function bbloomer_translate_woocommerce_strings( $translated, $text, $domain ) {
 
	// STRING 1
	$translated = str_ireplace( 'removed. Undo?', 'ON OFFER', $translated );
	$translated = str_ireplace( 'Product Description', 'Product Specifications', $translated); 
	$translated = str_ireplace( 'optional', 'optioneel', $translated );
	$translated = str_ireplace( 'Country', 'land', $translated );
	$translated = str_ireplace( 'Street address', 'adres', $translated );
	$translated = str_ireplace( 'House number and street name', 'House number and street name', $translated );
	$translated = str_ireplace( 'Apartment, suite, unit etc', 'toevoeging', $translated );
	$translated = str_ireplace( 'Postcode / ZIP', 'postcode', $translated );
	$translated = str_ireplace( 'Town/City', 'plaats', $translated );
	$translated = str_ireplace( 'Phone', 'telefoonnummer', $translated );
	$translated = str_ireplace( 'Email address', 'e-mailadres', $translated );
	$translated = str_ireplace( 'YOUR ORDER', 'jouw bestelling', $translated );
	
	$translated = str_ireplace( 'Select your bank', 'kies je bank', $translated );
	$translated = str_ireplace( 'Your personal data will be used to process your order, support your experience throughout this website, and for other purposes described in our', 'Je persoonlijke gegevens worden gebruikt om je order af te handelen, je gebruikservaring te verbeteren op de website, en voor andere doeleinden zoals beschreven in onze privacy policy.', $translated );
	$translated = str_ireplace( 'privacy policy', 'algemene voorwaarden', $translated );
	$translated = str_ireplace( 'terms and conditions', 'algemene voorwaarden', $translated );
	$translated = str_ireplace( 'I have read and agree to the website', 'ik heb de gelezen en ga ermee akkoord', $translated );
	$translated = str_ireplace( 'Place order', 'plaats je bestelling', $translated );
	$translated = str_ireplace( 'Please read and accept the terms and conditions to proceed with your order.', 'lees alsjeblieft de algemene voorwaarden gelezen en ga ermee akkoord om door te gaan met je bestelling', $translated );
	$translated = str_ireplace( 'BILLING DETAILS', 'contactgegevens', $translated );
	$translated = str_ireplace( 'ADDITIONAL INFORMATION', 'extra informatie', $translated );
	$translated = str_ireplace( 'Order notes', 'berichtje aan ons', $translated ); 
	$translated = str_ireplace( 'Notes about your order, e.g. special notes for delivery.', 'als je iets aan ons kwijt wilt kan je dat hier doen', $translated );
	$translated = str_ireplace( 'Bestaande klant? Log hier in', 'Bestaande klant? Log hier in', $translated );

	$translated = str_ireplace( 'Create an account?', 'wil je een account aanmaken?', $translated );
	$translated = str_ireplace( 'Account username', 'Account username', $translated );
	$translated = str_ireplace( 'Username', '', $translated ); 
	$translated = str_ireplace( 'Create account password', 'wachtwoord', $translated );
	$translated = str_ireplace( 'Password', '', $translated );
	$translated = str_ireplace( 'DATE', 'datum', $translated );
	$translated = str_ireplace( 'EMAIL', 'e-mailadres', $translated );
	//$translated = str_ireplace( 'TOTAL', 'totaal', $translated );
	$translated = str_ireplace( 'PAYMENT METHOD', 'betaalmethode', $translated );
	$translated = str_ireplace( 'Payment completed by', 'Payment completed by', $translated );
	$translated = str_ireplace( 'IBAN (last 4 digits)', 'IBAN (laatste 4 cijfers)', $translated );
	$translated = str_ireplace( 'Order details', 'jouw bestelling', $translated );
	$translated = str_ireplace( 'Payment method', 'betaalmethode', $translated );
	$translated = str_ireplace( 'Subtotal', 'subtotaal', $translated );
	//$translated = str_ireplace( 'Total', 'totaal', $translated );
	$translated = str_ireplace( 'ORDER NUMBER', 'ORDER NUMMER', $translated );
	$translated = str_ireplace( 'We have finished processing your order.', 'We hebben je bestelling ontvangen en gaan ermee aan de slag.', $translated );
	$translated = str_ireplace( 'Thanks for shopping with us.', 'Alvast veel plezier ermee!', $translated );
	$translated = str_ireplace( 'Coupon', 'voucher', $translated );
	$translated = str_ireplace( 'Remove', 'verwijderen', $translated );
	$translated = str_ireplace( 'Returning customer?', 'Bestaande klant?', $translated );
	$translated = str_ireplace( 'Click here to login', 'Log hier in', $translated );
	$translated = str_ireplace( 'Please read and accept the algemene voorwaarden to proceed with jouw bestelling', 'lees alsjeblieft de algemene voorwaarden gelezen en ga ermee akkoord om door te gaan met je bestelling', $translated );
	$translated = str_ireplace( 'This voucher has expired', 'Deze voucher is verlopen', $translated );
	$translated = str_ireplace( ' is a required field', 'is een verplicht veld', $translated );
	$translated = str_ireplace( 'is not a valid postcode', 'is geen geldige postcode', $translated );
	$translated = str_ireplace( 'Coupon  does not exist', 'helaas, deze voucher code wordt niet herkend', $translated ); 

	$translated = str_ireplace( 'Coupon code applied successfully', 'voucher code is met succes toegevoegd', $translated );
	$translated = str_ireplace( 'Thank you. jouw bestelling has been received', 'Dankjewel, je bestelling is ontvangen', $translated );
	$translated = str_ireplace( 'Notes about jouw bestelling, e.g. special notes for delivery', 'is een verplicht veld', $translated );
	

return $translated;
}

// WooCommerce Rename Checkout Fields
add_filter( 'woocommerce_checkout_fields' , 'custom_rename_wc_checkout_fields' );

// Change placeholder and label text
function custom_rename_wc_checkout_fields( $fields ) {
$fields['billing']['billing_first_name']['placeholder'] = '';
$fields['billing']['billing_first_name']['label'] = 'voornaam';
$fields['billing']['billing_last_name']['label'] = 'achternaam';
$fields['billing']['billing_company']['label'] = 'bedrijfsnaam';
return $fields;
}

add_filter( 'woocommerce_cart_subtotal', 'exclude_tax_subtotal', 15, 4 );
function exclude_tax_subtotal( $product_subtotal ) {
    // do whatever you want to do
    return $product_subtotal;
}

// Action to register admin menu
add_action( 'admin_menu', 'byp_register_menu' );
add_action ( 'admin_init','byp_register_settings' );

/**
 * Function to register admin menus
 */
function byp_register_menu() {

	//add_menu_page( 'Pixelate', 'pixelate', 'unfiltered_html', 'pixelate', '', '', 5);
	add_submenu_page( 'edit.php?post_type=pixelate', __('Admin', 'byp'), __('Admin', 'byp'), 'manage_options', 'pix-admin', 'byppix_admin_settings_page' );
	add_submenu_page( 'edit.php?post_type=pixelate', __('Customer', 'byp'), __('Customer', 'byp'), 'manage_options', 'pix-cust', 'byppix_cust_settings_page' );
}

function byppix_cust_settings_page() {
	require get_template_directory()."/byp-pixcust.php";	
}

function byp_register_settings(){
	register_setting( 'byp_pix_options', 'byp_pixelate_data' );
	register_setting( 'byp_pix_border', 'byp_border_data');
}

/**
 * Function to register post type: recipe
 * 
 * @package Lecolefood
 * @since 1.0.0
 */
function pix_register_post_type() {
	
	$rcp_labels = apply_filters('rcp_post_labels', array(
		'name'                 	=> __('Pixelate', 'lecolefood'),
		'singular_name'        	=> __('Pixelate', 'lecolefood'),
		'add_new'              	=> __('Add Pixelate', 'lecolefood'),
		'add_new_item'         	=> __('Add Pixelate', 'lecolefood'),
		'edit_item'            	=> __('Edit Pixelate', 'lecolefood'),
		'new_item'             	=> __('Pixelate', 'lecolefood'),
		'view_item'            	=> __('View Pixelate', 'lecolefood'),
		'search_items'         	=> __('Search Pixelate','lecolefood'),
		'not_found'            	=> __('No Pixelate found', 'lecolefood'),
		'not_found_in_trash'   	=> __('No Pixelate found in Trash', 'lecolefood'),
		'menu_name' 			=> __('Pixelate', 'lecolefood'),
		'featured_image'		=> __('Pixelate Image', 'lecolefood'),
		'set_featured_image'	=> __('Set Pixelate Image', 'lecolefood'),
		'remove_featured_image'	=> __('Remove Pixelate Image', 'lecolefood'),
		'use_featured_image'	=> __('Use as Pixelate Image', 'lecolefood'),
	));
	
	$rcp_args = array(
		'labels'              => $rcp_labels,
		'public'              => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'show_ui'             => true,
		'show_in_menu'        => true, 
		'query_var'           => true,
		'rewrite'             => array( 
										'slug' 			=> apply_filters('pixelate-options', 'recipes'),
										'with_front' 	=> false
									),
		'capability_type'     	=> 'page',
		'has_archive'         	=> false,
		'hierarchical'        	=> false,
		'menu_icon'   			=> 'dashicons-nametag',
		'supports'            	=> apply_filters('rcp_post_supports', array('page-attributes'))
	);

	// Register team showcase post type
	register_post_type( 'pixelate', apply_filters('rcp_registered_post', $rcp_args) );
}
// Action to register plugin post type
add_action('init', 'pix_register_post_type');
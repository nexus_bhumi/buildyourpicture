<?php
/**
 * Template Name: Bricks Image
 *
 * Description: Twenty Twelve loves the no-sidebar look as much as
 * you do. Use this page template to remove the sidebar from any page.
 *
 * Tip: to remove the sidebar from all posts and pages simply remove
 * any active widgets from the Main Sidebar area, and the sidebar will
 * disappear everywhere.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header();
wp_enqueue_style( 'bricks-img-style');
wp_enqueue_script( 'bricks-img-edit');

wp_enqueue_script( 'cropper-min-js');
wp_enqueue_style( 'cropper-min-css'); 

wp_enqueue_style( 'jquery-ui-css');
wp_enqueue_script( 'jquery-ui-js');
//wp_enqueue_script( 'jquery-imgflip-js');
//wp_enqueue_script( 'jquery-caman-js');
wp_enqueue_script( 'jquery-pixelate-js');

wp_enqueue_style( 'cropper-css'); 
//wp_enqueue_style( 'main-css'); 
//wp_enqueue_style( 'bootstrap-min-css'); 

//wp_enqueue_script( 'jquery-slim-min-js');
//wp_enqueue_script( 'bootstrap-bundle-min-js');
wp_enqueue_script( 'cropper-js');

wp_enqueue_script( 'main-js');


?>


<style type="text/css">
  .hidden{ display: none; }
  .byp_grayscale{  -webkit-filter: grayscale(100%); /* Safari 6.0 - 9.0 */
  filter: grayscale(100%); }

  #byp_img_title { color: #ffffff; font-weight: 600; font-size: 12px; }

</style>

<div class="uploard-box">
    <div class="upload-file-section">
      <div class="uload-box-left uploadlfttxt ">
         <h2 style="text-align: center;">Select the image from our gallery<br>OR<br>Upload your own picture.</h2>
      </div>
      <div class="uload-box-right" id="drop-area">
        <div class="vc_single_image-wrapper   vc_box_border_grey">
          <img width="377" height="350" src="https://mydemoprojects.com/buildyourpicture/wp-content/uploads/2019/05/upload-2.png" class="vc_single_image-img attachment-full" alt="" </div>
        </div>
        <div class="uploadtxt">

          <p>Drag &amp; Drop Files or<br> 

            <div class="file-uploard">

              <input type="file" id="file-input" class="upload-btn"  multiple >
              <!-- onchange="handleFiles(this.files)" -->

              <a id="upload-btn"   href="#">Browse</a>
            </div>
           
          </p>
          
        </div> 
         <span id="byp_img_title"></span>
            <!--  <progress id="progress-bar" max=100 value=0></progress> -->
          <!-- <div id="gallery" /></div> -->
      </div>
    </div>
    <div class="start-btn-section">
      <a  class="btn-start" href="#" title="START NOW"><i class="vc_btn3-icon fa fa-upload"></i> Start!</a>
    </div>  
</div>


<section class="upload-image1">
  <div class="middle-container">
    <div class="upload-heading-txt">
      <h3>Here you can select which part of the picture you want us to create a mosaic from. Also select the mosaic-size vou want. Etc.</h3>
    </div>

    <div class="" id="drag_file"></div>

    <div class="image-editors1">
      <div class="sidebar-editors">
        <div class="editable-listing editable-crop ">
          <span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/crop-icon.png">Crop</span>
          <div class="crop-data">
            <div class="cropping">
              <div class="scroll">
                <ul id="crop_ratio_list" class="crop_ratio_list"> 
                  <!-- <li id="byp_data_ratio" >
                    <label> 30x30</label> 
                    <div class="ractangle sel_pixel" style="width: 30px; height: 30px;"></div>
                  </li> -->
                  <?php  
                  global $post, $page;

                  if( have_rows('pixalete_ratio') ): ?>
                    

                    <?php while( have_rows('pixalete_ratio') ): the_row(); 

                    // vars
                    $width = get_sub_field('width');
                    $height = get_sub_field('height'); 

                    if(!empty($width) && !empty($height)) { ?>             


                         <li id="byp_data_ratio" data-w="<?php  echo $width; ?>" data-h="<?php echo $height; ?>">
                          <label><?php  echo $width; ?>x<?php echo $height; ?></label> 
                          <div class="ractangle" style="width: <?php  echo $width; ?>px;height: <?php echo $height; ?>px;">
                          </div>
                        </li>

                    <?php } ?>            
                    <?php endwhile; ?>  
                  <?php endif; ?>          
                </ul>
              </div>
            </div>
            <div class="crop-btn">
              <input type="hidden" name="crop_ratio_pixel" id="crop_ratio_pixel" min="" max="">
              <button id="byp_apply_crop">Next</button>
            </div>
          </div>
        </div>
        <div class="editable-listing editable-effect selective-section">
          <span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/effect-icon.png">Effects</span>
          <div class="effect-box">
            <div class="effect-box-effect editable-britness">
              <span>Brightness</span>
              <div class="range"><input name = "byp_adj_range" id="byp_brightness" class="range-custom"  type="range" min="0.2" max="2.2" step="0.2" value="1.2"></div>
            </div>
            <div class="effect-box-effect editable-contrest">
              <span>Contrast</span>
              <div class="range"><input name = "byp_adj_range" id="byp_contrast" class="range-custom"  type="range"  min="0.2" max="2.2" step="0.2" value="1.2"></div>
            </div>
            <div class="effect-box-effect editable-saturation">
              <span>Saturation</span>
              <div class="range"><input name = "byp_adj_range" id="byp_saturation" class="range-custom"  type="range"  min="0.2" max="2.2" step="0.2" value="1.2"></div>
            </div>
          </div>
        </div>
       
        <div class="editable-listing editable-transform selective-section">
          <span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/transform-icon.png">Transform</span>         
          <div class="transform-box">
            <div class="rotate-box">
              <span>Rotate</span>
              <div class="rotate-btn">
                <button title="rotate-left" id="clockwise"><i class="fa fa-undo" aria-hidden="true"></i></button>
                <button title="rotate-right" id="counterclockwise"><i class="fa fa-repeat" aria-hidden="true"></i></button>  
              </div>
            </div>
            <div class="rotate-box">
              <span>Flip</span>
              <div class="rotate-btn flip-btn">
                <button id="mirrorv"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/flip.png" title="Flip Horizontal "></button>
                <button id="mirrorh"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/flip-2.png" title="Flip Vertical"></button>
              </div>
            </div>
            <div class="grayscale-box">
              <span>Grayscale</span>
              <div class="grayscale-btn">
                <button id="byp_grayscale" class="btn-grayscale" data-title="" title="Reset">Grayscale</button>
              </div>
            </div>
          </div>
        </div>
        
      </div>

      <div class="editable-output">
        <div class="output-image-box">
          <div  class="orignal-iamge">
            <label>Original Image</label>
            <div class="process-work-image img-container"> 
              <img id="img1" src="">
              <div class="inti_image">
              </div>           
               <!-- <img id="img1" src=""> -->
              <!-- <img src="https://mydemoprojects.com/buildyourpicture/wp-content/uploads/2019/05/Dining_feifeiwanton.jpg"> -->
            </div>
          </div>
          <div class="preview-image">
            <label>Preview Image</label>
            <div class="process-output-image img-preview preview-lg ">            
              <!-- <img id="image2" src="">
              <canvas id="new_canvas"></canvas> -->
              <!-- <img id="image2" src="https://mydemoprojects.com/buildyourpicture/wp-content/uploads/2019/05/Dining_feifeiwanton.jpg"> -->
              <img id="image2" src="">
          
          
              <canvas id="new_canvas"></canvas>
              <!-- <canvas id="new_show_canvas" style="display: none;"></canvas> -->
            </div>          
          </div>
        </div>
        <div class="output-buttons">
          <div class="submit-button">
            <button id="pixelet" class="btn-pixalet selective-section" title="Pixalet" >Pixlate</button>
            <button class="btn-reset" title="Reset" id="byp_reset">Start Again</button>
            <button class="btn-price" title="Reset">$25</button>
            <button id="add_to_order" class="btn-add-order" data-title="" title="Reset">Add to cart</button>
          </div>
        </div>
      </div>    
    </div>
  </div>
</section>
<script type="text/javascript">
  jQuery(".crop_ratio_list li").click(function() {
    jQuery(".crop_ratio_list li")
      .not(this)
      .removeClass("active");
    jQuery(this).addClass("active");
  });  
 jQuery(".selective-section").find("*").prop("disabled", true);
 jQuery(".crop-btn button").click(function() {
    jQuery(".selective-section").find("*").prop("disabled", false);
    jQuery(".selective-section").css("cursor", "pointer");
 });

</script>

<?php 
//8595






get_footer();  ?>
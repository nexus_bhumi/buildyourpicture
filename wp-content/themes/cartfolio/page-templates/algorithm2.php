<?php 

/*
 * Template Name: Algorithm
 *
 * Description: Twenty Twelve loves the no-sidebar look as much as
 * you do. Use this page template to remove the sidebar from any page.
 *
 * Tip: to remove the sidebar from all posts and pages simply remove
 * any active widgets from the Main Sidebar area, and the sidebar will
 * disappear everywhere.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

 get_header();
?>

   
        
        <style type="text/css">
            body > div {
                width: 960px;
                margin: auto;
            }

            .left-sidebar .main-content-inner{
            	background: none !important;
            }

            h1 {
                font-family: sans-serif;
                border-bottom: 1px solid black;
            }

            p {
                font-size: 150%;
            }

            h3 {
                margin-bottom: 0;
            }

            .palette span {
                display: inline-block;
                height: 30px;
                width: 30px;
                margin-right: 10px;
            }

            .sample {
                height: 300px;
            }

            .actual,
            .nearest {
                position: relative;
                display: inline-block;
                width: 50%;
                height: 100%;
            }

            .actual:before {
                position: absolute;
                display: block;
                content: "Actual";
                top: calc(50% - 10px);
                left: 0;
                right: 0;
                line-height: 20px;
                text-align: center;
            }

            .nearest:before {
                position: absolute;
                display: block;
                content: "Nearest";
                top: calc(50% - 10px);
                left: 0;
                right: 0;
                line-height: 20px;
                text-align: center;
            }
        </style>

        <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    
        <div>
            <h1>nearest-color</h1>



            <form>
                <h3> Enter RGB (i.e. 255,255,255)

</h3>

                <input  id="colorenter" />
                <a  id="btcolor" onclick="ClickColor();">Find</a>
                </br>

            </form>



            <div class="palette" id="alternate-palette">
                <h3>Available Colors</h3>


                <span style="background-color: rgb(251, 94, 22);"></span>
                <span style="background-color: rgb(247, 205, 2);">
                </span><span style="background-color: rgb(126, 155, 22);">
                </span><span style="background-color: rgb(235, 11, 11);"></span>
                <span style="background-color: rgb(127, 18, 19);"></span>
                <span style="background-color: rgb(252, 175, 227);"></span>
                <span style="background-color: rgb(43, 25, 84);"></span>
                <span style="background-color: rgb(9, 58, 132);"></span>
                <span style="background-color: rgb(0, 146, 215);"></span>
                <span style="background-color: rgb(96, 175, 216);"></span>
                <span style="background-color: rgb(243, 213, 135);"></span>
                <span style="background-color: rgb(16, 120, 32);"></span>
                <span style="background-color: rgb(231, 185, 97);"></span>
                <span style="background-color: rgb(68, 32, 19);"></span>
                <span style="background-color: rgb(179, 110, 46);"></span>
                <span style="background-color: rgb(250, 250, 250);"></span>
                <span style="background-color: rgb(172, 172, 172);"></span>
                <span style="background-color: rgb(55, 55, 55);"></span>
                <span style="background-color: rgb(5, 5, 5);"></span></div>
        </div>

        <div class="sample" id="alternate-sample">
            <div class="colorcodehexa"></div>
            <div class="nearest"></div>
        </div>

    </div>

    <script>


        function ClickColor()
        {
            var colorcode = "#107820,#FB5E16,#F7CD02,#7E9B16,#EB0B0B,#7F1213,#FCAFE3,#2B1954,#093A84,#0092D7,#60AFD8,#F3D587,#E7B961,#442013,#B36E2E,#FAFAFA,#ACACAC,#373737,#050505";

            var colorarray = colorcode.split(',');

            var ss = $("#colorenter").val()
            var index = -1;
            var sdis = 125655445;
            var datas = ss.split(',');

            var xyzs = converte_RGB_to_XYZ(datas)
            var cielbs = converte_XYZ_to_CIELAB(xyzs);

            for (var i = 0; i < $("#alternate-palette").children().length; i++)
            {

                if (i != 0) {
                    var datad = $("#alternate-palette").children()[i].style.backgroundColor.replace("rgb(", "").replace(")", "").replace(" ", "").split(',');

                    var xyzd = converte_RGB_to_XYZ(datad)
                    var cielbd = converte_XYZ_to_CIELAB(xyzd);


                    var dis = Math.sqrt(((cielbs[0] - cielbd[0]) * (cielbs[0] - cielbd[0])) + ((cielbs[1] - cielbd[1]) * (cielbs[1] - cielbd[1])) + ((cielbs[2] - cielbd[2]) * (cielbs[2] - cielbd[2])));
                    //console.log(dis);
                    if (dis < sdis)
                    {
                        index = i;
                        sdis = dis;
                    }
                }
            }


            $(".nearest").css("background-color", colorarray[index - 1]);
            console.log(colorarray[index - 1]);
            $(".colorcodehexa").html(colorarray[index - 1]);

            return;

        }

        function converte_RGB_to_XYZ(umVetor) {

            var r = umVetor[0] / 255;
            var g = umVetor[1] / 255;
            var b = umVetor[2] / 255;
            if (r > 0.04045)
                r = Math.pow((r + 0.055) / 1.055, 2.4);
            else
                r = r / 12.92;
            if (g > 0.04045)
                g = Math.pow((g + 0.055) / 1.055, 2.4);
            else
                g = g / 12.92;
            if (b > 0.04045)
                b = Math.pow((b + 0.055) / 1.055, 2.4);
            else
                b = b / 12.92;
            r = 100 * r;
            g = 100 * g;
            b = 100 * b;
            var X = 0.4124 * r + 0.3576 * g + 0.1805 * b;
            var Y = 0.2126 * r + 0.7152 * g + 0.0722 * b;
            var Z = 0.0193 * r + 0.1192 * g + 0.9505 * b;
            return [X, Y, Z];
        }


        function converte_XYZ_to_CIELAB(umVetor) {
            var X = umVetor[0] / 95.047;
            var Y = umVetor[1] / 100.0;
            var Z = umVetor[2] / 108.883;
            var L, a, b;
            if (X > 0.008856)
                X = Math.pow(X, 1 / 3);
                //else X = (903.3 * X + 16) / 116;
            else
                X = (7.787 * X) + (16 / 116)
                //else X = (841.0/108.0)*(X) + (4.0/29.0);
            if (Y > 0.008856)
                Y = Math.pow(Y, 1 / 3);
                //else Y = (903.3 * Y + 16) / 116;
            else
                Y = (7.787 * Y) + (16 / 116)
                //else Y = (841.0/108.0)*(Y) + (4.0/29.0);
            if (Z > 0.008856)
                Z = Math.pow(Z, 1 / 3);
                //else Z = (903.3 * Z + 16) / 116;
            else
                Z = (7.787 * Z) + (16 / 116)
                //else Z = (841.0/108.0)*(Z) + (4.0/29.0);
            L = 116 * Y - 16;
            a = 500 * (X - Y);
            b = 200 * (Y - Z);
            return [L, a, b];
        }


    </script>


<?php

/**
 * Template Name: Latest demo sanjay
 *
 * Description: This page template creates image edit layout with all the quick options
 * by using that user can edit image to place new order.
 *
 * @package WordPress
 * @subpackage cartfolio
 * @since Twenty Twelve 1.0
 */

get_header();

wp_enqueue_style( 'custom-img-edit');
wp_enqueue_style( 'cropper-min-css');
wp_enqueue_script( 'cropper-min-js');
wp_enqueue_style( 'jquery-ui-css');
wp_enqueue_script( 'jquery-ui-js');
wp_enqueue_script( 'jquery-pixelate-js');
wp_enqueue_script( 'cust-crop-img-js');
?>
<style type="text/css">
  .byp-overlay{display: none;}
  #byp_succ_cart{ color: #c7232 !important; text-align: center; line-height: 15px; margin-top: 17px;}
  .loader {
    position: absolute;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('<?php echo get_stylesheet_directory_uri(); ?>/images/byp-loader.gif') 50% 50% no-repeat;
        background-size: auto;
    display: none;
    background-size: 50px 50px;
}
.load-prev{position: fixed !important; background-image: none!important; background-color: rgba(0,0,0,0.4);  }

.btn_csv{
      margin-top: 10px;
    border-radius: 7px;
    background: #c72329 !important;
    box-shadow: 0px 4px 0px 0px rgba(137, 21, 25, 1) !important;
    -webkit-box-shadow: 0px 4px 0px rgba(137, 21, 25, 1) !important;
    width: 110px !important;
    height: 42px;
    padding: 0 !important;
    line-height: 18px;
    font-size: 12px !important;
    letter-spacing: 0;
}

.btn_csv a{color: white; }
.loader-addtocart {

      position: relative;
    left: 0px;
    top: -4px;
    width: 50px;
    height: 50px;
    z-index: 9999;
   background: url('<?php echo get_stylesheet_directory_uri(); ?>/images/byp-loader.gif') 50% 50% no-repeat;
        background-size: auto;
    background-size: auto;
    display: none;
    background-size: 30px 30px;   
}
</style>
<section class="upload-image1">
  
  <div class="middle-container upload-container">
    <div class="upload-heading-txt">
      <h3>Kies een afbeelding die je wilt namaken met bouwsteentjes! Selecteer het perfecte gedeelte van je afbeelding en pas 'm naar wens aan.</h3>
    </div>
    <div class="" id="drag_file"></div>
    <div class="image-editors1">
     
      <div class="editable-output">
        <div class="output-image-box">
          <div  class="orignal-iamge">
            <label>Jouw afbeelding</label>
            <?php 
           // var_dump(wp_get_attachment_metadata(108));
            ?>
            <div class="start-over-section">
              <button class="start-over" id="byp_start_ovr"><i class="fa fa-trash" aria-hidden="true"></i><span>begin opnieuw</span></button>
              
            </div>
            <div class="process-work-image"> 
              <div class="uploadtxt" id="drop-area">   
                <div class="upload-files-box">            
                  <div class="file-uploard">
                    <input type="file" id="file_input_1" class="upload-btn"  multiple >                   
                    <a id="upload-btn" class="choose-your-photo"   href="#"></a>
                  </div>
                  <h5>OF</h5>
                  <div class="file-uploard" id="wp-content-media-buttons">
                   
                 <?php echo do_shortcode( '[popup_anything id="9220"]' ); ?>

                  </div>
                </div>
              </div> 

              <img id="byp_ini_img" src="">
              <div class="inti_image" style="display: none;">
                <img id="byp_ori_img" src="">
              </div> 
              
            </div>
          </div>
          <div class="preview-image">
            <label>Eindresultaat</label>
            <div class="loader mobilehide"></div>
            <div class="start-over-section">
             
            </div>
            <div class="process-output-image"> 
              <div class="byp-overlay"></div>
              <div class="loader load-prev mobileloader"></div>
              <img id="byp_prev_img" src="https://mydemoprojects.com/buildyourpicture/wp-content/uploads/2019/05/preview.png">  
              <img src="" id="byp_tmp_cropped" style="display: none;">           
             <canvas id="byp_temp_canvas" style="display: none;"></canvas>
             <canvas id="byp_temp_canvas_temp" style="display: none;"></canvas>
             
            </div>  
          </div>
        </div>
        <div class="output-buttons" id="byp_action_btn">
          <div class="submit-button">
            
            <label class="btn-price" title="">
              <span>€</span><span id="byp_pro_price">25</span> 
            </label>
           <button id="byp_add_order" class="btn-add-order" data-title="" >Voeg toe aan winkelmand</button>
           <span id="byp_succ_cart"></span>
           <span class="loader-addtocart"></span>
       
         <div class="woocommerce-variation-add-to-cart variations_button">
                <button style="display: none;" type="submit" class="custom_add_to_cart single_add_to_cart_button button btn-add-order">Voeg toe aan winkelmand</button>
                <input type="hidden" name="add-to-cart" class="addtocartv" value="" />
                <input type="hidden" name="product_id" class="addtocartv" value="" />
                <input type="hidden" name="variation_id" class="variation_id" value="0" />
               
            </div>

          </div>
         
          <div class="product-quantity" data-title="Quantity">
           
            <div class="quantity">
              <span class="tmpmela-quantity">Aantal: </span>
              
                <div class="pt_Quantity kk">
                  <input type="number" min="1" max="100" id="byp_qty" step="1" pattern="[0-9]*" title="Aantal" value="1" name="cart[0023a1e3447fdb31836536cc903f1310][qty]" data-inc="1" autocomplete="off">
                </div>
            </div>           
          </div>

          <!--  download csv -->
          <button id="btn_csv" class="btn_csv" data-title="" ><a id="a_csv" href="javascript:;">Download CSV</a> </button> 
        <!-- </form> -->
        </div>
      </div>  

      <button  id="byp_apply_crop" style="display: none;"></button>     
      
      <div class="sidebar-editors">      
        
          <div class="editable-listing editable-crop selective-section">
            <span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/crop-icon2.png">Afmetingen</span>
            <div class="crop-data">            
                
              <ul id="crop_ratio_list" class="crop_ratio_list"> 
               
                <?php  
                global $post, $page;
                $maxwidth = array();
                if( have_rows('pixalete_ratio') ): ?>   
                  <?php while( have_rows('pixalete_ratio') ): the_row(); 
                  // vars
                  $width = get_sub_field('width');
                  $height = get_sub_field('height'); 
                  if($width == $height){
                    $maxwidth[] = $width;
                  }
                  if(!empty($width) && !empty($height)) { ?> 
                    <!---  active ma "selected-crop" no class added  -->
                       <li id="byp_data_ratio" class="byp_data_ratio" data-w="<?php  echo $width; ?>" data-h="<?php echo $height; ?>">
                          <div class="crop-box">                      
                            <div class="ractangle" style="width: <?php  echo $width; ?>px;height: <?php echo $height; ?>px;"></div>
                          </div>
                          <label><?php  echo $width; ?>x<?php echo $height; ?></label> 
                                              
                      </li>
                  <?php } ?>
                  <div class="clearfix"></div> 
                  <?php endwhile; ?>  
                <?php endif; ?>   
              </ul>  
            </div>
          </div>
          
          <!--  hidden fields -->
          <input type="hidden" name="crop_ratio_pixel" id="crop_ratio_pixel" min="50" max="50">
          <input type="hidden" name="byp_rotate" id="byp_rotate" value="">
          <input type="hidden" name="byp_mirrorh" id="byp_mirrorh" value="">
          <input type="hidden" name="byp_mirrorv" id="byp_mirrorv" value="">
          <input type="hidden" name="byp_max_width" id="byp_max_width" value="<?php echo max($maxwidth); ?>">
          <input type="hidden" name="byp_img_title" id="byp_img_title" value="">
          <input type="hidden" name="byp_edit_cimg" id="byp_edit_cimg" value="">
          <input type="hidden" name="byp_csv_data" data-json='' id="byp_csv_data" value="">

          <div id="bypmodal" class="modal">
            <!-- Modal content -->
            <div class="modal-content">
              <span class="close">&times;</span>
              <p>Kies eerst een afbeelding...</p>
            </div>
          </div>

          <div id="bypmodaladdcartclik" class="modal popupbuns">
            <!-- Modal content -->
            <div class="modal-content">
              <span class="close">&times;</span>
              <p id="byp_prod_added">Hoppa, het product zit in je winkelmand!</p>
              <div class="buttons-viewcart">
                <?php global $woocommerce;
                $cartUrl = $woocommerce->cart->get_cart_url();
                ?>
                <a href="<?php echo $cartUrl; ?>" class="button viewcrt">Naar je winkelmand</a><a href="#" class="button editimgbtn">Sluiten</a>
              </div>

            </div>
          </div>

          <div class="editable-listing editable-effect selective-section">
            <span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/effect-icon2.png">Effecten</span>
            <div class="effect-box">
              <div class="effect-box-effect editable-brightness">
                <label>Helderheid</label>
                <div class="range"><input name = "byp_adj_range" id="byp_brightness" class="range-custom"  type="range" min="-100" max="100" step="1" value="0"></div>
                <strong id="brightness">0</strong>
              </div>
              <div class="effect-box-effect editable-contrest">
                <label>Contrast</label>
                <div class="range"><input name = "byp_adj_range" id="byp_contrast" class="range-custom"  type="range" min="-100" max="100" step="1" value="0"></div>
                <strong id="contrest">0</strong>
              </div>
              <div class="effect-box-effect editable-saturation">
                <label>Intensiteit</label>
                <div class="range"><input name = "byp_adj_range" id="byp_saturation" class="range-custom"  type="range" min="-100" max="100" step="1" value="0"></div>
                <strong id="saturation">0</strong>
              </div>
            </div>
          </div>  
          <div class="editable-listing editable-transform selective-section1">
            <span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/transform-icon2.png">Aanpassingen</span> 
            <div class="transform-box">
              <div class="rotate-box">
                <label>Draaien</label>
                <div class="rotate-btn">
                  <button title="rotate-left" id="counterclockwise"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/rotate-left.png" title="rotate Left "></button>
                  <button title="rotate-right" id="clockwise"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/rotate-right.png" title="rotate Right "></button>  
                </div>
              </div>
              <div class="rotate-box">
                <label>Spiegelen</label>
                <div class="rotate-btn flip-btn">
                  <button id="mirrorh"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/flip-horizontal.png" title="Flip Horizontal "></button>
                  <button id="mirrorv"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/flip-vertical.png" title="Flip Vertical"></button>
                </div>
              </div>
              <div class="grayscale-box selective-section">
                <label class="radio-custom">Kleur
                  <input type="radio" checked="checked" name="color_gray_radio"  value="fullcolor">
                  <span class="checkmark"></span>
                </label>
                <label class="radio-custom">Zwart-wit
                  <input type="radio" name="color_gray_radio"  value="gray">
                  <span class="checkmark"></span>
                </label>
              </div>
            </div>
          </div> 
       
      </div>
    </div>
  </div>
</section>

<?php 
$id=8297; 
$post = get_post($id); 
$content = apply_filters('the_content', $post->post_content); 
echo $content;  
?>
<?php
  //cred_form("Rediger ".$post->post_type,$post->ID);    //e.g "Rediger nyheter" ("edit news")
?>
       
<?php
       // wp_enqueue_media(); // needed to get the wp-object loaded
        //wp_register_script( 'nb-media-uploader' , plugins_url('js/media.js',__FILE__) , array( 'jquery' ) );
        //wp_enqueue_script( 'nb-media-uploader' );            
?>
<script type="text/javascript">

jQuery(document).ready( function() {

  jQuery("#byp_start_ovr").hide();
  jQuery("#byp_action_btn").hide();

  jQuery(".crop_ratio_list li").click(function() {
    jQuery(".crop_ratio_list li")
      .not(this)
      .removeClass("selected-crop ");
    jQuery(this).addClass("selected-crop ");
  }); 

  jQuery(".crop_ratio_list, .radio-custom, .range, .transform-box button").addClass('byp_no_click');  
  jQuery(".selective-section").find("*").prop("disabled", true);

  jQuery(".crop_ratio_list, .range, .rotate-btn .byp_no_click, .grayscale-box .radio-custom").click(function(){
    if(jQuery(this).hasClass( "byp_no_click" )) { get_popup(); }
  });
});

function get_popup(){
  var img_src = jQuery("#byp_ini_img").attr("src");
  if(img_src == ''){
     var modal = document.getElementById("bypmodal");  
 
  var span = document.getElementsByClassName("close")[0];
  modal.style.display = "block";

  // When the user clicks on <span> (x), close the modal
  span.onclick = function() {
    modal.style.display = "none";
  }

  // When the user clicks anywhere outside of the modal, close it
  window.onclick = function(event) {
    if (event.target == modal) {
      modal.style.display = "none";
    }
  }
}
 
}

jQuery( document ).ready(function() {  
  
  var isMobile = window.matchMedia("only screen and (max-width: 767px)").matches;        
  if (isMobile) {    
    height_tri = jQuery( ".process-work-image" ).width();
    jQuery(".process-work-image").css("height",height_tri);  
    jQuery(".process-output-image").css("height",height_tri); 
  }
});

</script>

<script type="text/javascript">
  (function($) {
  "use strict";

  function customQuantity() {
    /** Custom Input number increment js **/
    jQuery(
      '<div class="pt_QuantityNav"><div class="pt_QuantityButton pt_QuantityUp">+</div><div class="pt_QuantityButton pt_QuantityDown">-</div></div>'
    ).insertAfter(".pt_Quantity input");
    jQuery(".pt_Quantity").each(function() {
      var spinner = jQuery(this),
        input = spinner.find('input[type="number"]'),
        btnUp = spinner.find(".pt_QuantityUp"),
        btnDown = spinner.find(".pt_QuantityDown"),
        min = input.attr("min"),
        max = input.attr("max"),
        valOfAmout = input.val(),
        newVal = 0;

      btnUp.on("click", function() {
        var oldValue = parseFloat(input.val());

        if (oldValue >= max) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue + 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
      });
      btnDown.on("click", function() {
        var oldValue = parseFloat(input.val());
        if (oldValue <= min) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue - 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
      });
    });
  }
  customQuantity();
})(jQuery);

</script>

<?php 
get_footer();  ?>
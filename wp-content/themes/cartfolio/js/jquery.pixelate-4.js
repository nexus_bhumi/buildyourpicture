/*!

Name: highlightReel
Dependencies: jQuery
Author: Justin Duke
Author URL: http://jmduke.com
Github URL: https://github.com/jmduke
Licensed under the MIT license
*/

(function ($) {
  $.fn.pixelate = function (options) {
    // TODO:  There's, like, definitely more customization that we could do here.
    var defaults = {
        focus: 1,
        canvasID: "newCanvas"
      },
      settings = $.extend({}, defaults, options);

    this.each(function (index) {
      // Since we pretty much remove the <img> element from the picture pretty quickly, we need to
      // capture the important attributes before anything else.
      var preview_height = jQuery("#byp_tmp_cropped").height();
      var preview_width = jQuery("#byp_tmp_cropped").width();
      var container_height = jQuery(".process-output-image").height() + 4;
      var container_width = jQuery(".process-output-image").width() + 4;

      var newheight = 0;
      var newwidth = 0;

      if (preview_height == preview_width) {
        newheight = container_height;
        newwidth = container_height;
      } else if (preview_height > preview_width) {
        newheight = container_height;
        newwidth = (preview_width * container_height) / preview_height;
      } else if (preview_height < preview_width) {
        newwidth = container_width;
        newheight = (preview_height * container_width) / preview_width;
      }

      var width = newwidth;
      var height = newheight;

      /*var width = this.width;
      var height = this.height;*/

      var idSelector = "#" + settings.canvasID + index;

      // Hide the image itself, since we're transposing over it.
      $(this).hide();

      // Edge case: in multiple invocations of pixelate(), be sure to get rid of previous attempts.
      $(this)
        .parent()
        .children("canvas")
        .hide();
      $("#newCanvas0").remove();
      $(this).after("<canvas id='" + idSelector.slice(1) + "'>");

      $(idSelector).get(0).width = width;
      $(idSelector).get(0).height = height;

      var context = $(idSelector)
        .get(0)
        .getContext("2d");

      // This is how the whole thing works.
      context.imageSmoothingEnabled = false;
      context.mozImageSmoothingEnabled = false;
      context.webkitImageSmoothingEnabled = false;

      function pixelate(image) {
        var el = $("#" + settings.canvasID + index).get(0);

        var relativeWidth = settings.focus1;
        var relativeHeight = settings.focus2;

        context.drawImage(image, 0, 0, relativeWidth, relativeHeight);
        context.drawImage(
          el,
          0,
          0,
          relativeWidth,
          relativeHeight,
          0,
          0,
          el.width,
          el.height
        );

        var imgData = context.getImageData(0, 0, el.width, el.height);
        var newwidth = Math.round(290 / relativeWidth);
        var newheight = Math.round(290 / relativeHeight);

        var halfwidth = Math.round(newwidth / 2);
        var halfheight = Math.round(newheight / 2);

        var arr_x = [];
        var x_width = 0;
        for (x = 1; x <= relativeWidth; x++) {
          if (x == 1) {
            x_width = x_width + halfwidth;
          } else {
            x_width = x_width + newwidth;
          }
          arr_x.push(x_width);
        }

        var arr_y = [];
        var y_height = 0;
        for (y = 1; y <= relativeHeight; y++) {
          if (y == 1) {
            y_height = y_height + halfheight;
          } else {
            y_height = y_height + newheight;
          }
          arr_y.push(y_height);
        }

        var perlinepixel = 290;
        var arr_final = [];
        $.each(arr_y, function (index1, value1) {
          $.each(arr_x, function (index2, value2) {
            var position_y = (parseInt(perlinepixel) * parseInt(value1)) + parseInt(value2);
            arr_final.push(position_y);
          });

        });

        var count = 0;
        var newcount = 0;
        var unique_colors = [];
        var dict = {};
        for (i = 0; i < imgData.data.length; i += 4) {
          if ($.inArray(count, arr_final) != -1) {
            r = imgData.data[i].toString(16);
            g = imgData.data[i + 1].toString(16);
            b = imgData.data[i + 2].toString(16);

            if (r.length == 1) r = "0" + r;
            if (g.length == 1) g = "0" + g;
            if (b.length == 1) b = "0" + b;

            var finalcolor = "#" + r + g + b;
            unique_colors.push(finalcolor);

            var finalrgb = r + g + b;
            dict[newcount] = {
              "rgb": finalrgb,
              "color": finalcolor
            };
            newcount++;
          }
          count++;
        }
        //console.log(unique_colors);
        //console.log(dict);

        var all_colors_rgb = [16, 120, 32, 251, 94, 22, 247, 205, 02, 126, 155, 22, 235, 11, 11, 127, 18, 19, 252, 175, 227, 43, 25, 84, 09, 58, 132, 00, 146, 215, 96, 175, 216, 243, 213, 135, 231, 185, 97, 68, 32, 19, 179, 110, 46, 250, 250, 250, 172, 172, 172, 55, 55, 55, 05, 05, 05];
        var all_colors_hex = [];

        for (i = 0; i < all_colors_rgb.length; i += 3) {
          r = all_colors_rgb[i].toString(16);
          g = all_colors_rgb[i + 1].toString(16);
          b = all_colors_rgb[i + 2].toString(16);

          if (r.length == 1) r = "0" + r;
          if (g.length == 1) g = "0" + g;
          if (b.length == 1) b = "0" + b;

          var finalcolor = "#" + r + g + b;
          all_colors_hex.push(finalcolor);
        }
        //console.log(all_colors_hex);

        var res = unique_colors;
        var all_nearest_colors = [];
        $.each(res, function (ind, str) {
          var min = 0xffffff;
          var best, current, i;
          for (i = 0; i < all_colors_hex.length; i++) {
            current = dist(all_colors_hex[i], str)
            if (current < min) {
              min = current
              best = all_colors_hex[i];
            }
          }
          all_nearest_colors.push(best);
        });
        //console.log(all_nearest_colors);
        var full_obj = [];
        $.map(dict, function (val, key) {
          full_obj[key] = {
            "rgb": val.rgb,
            "color": val.color,
            "nearest_color": all_nearest_colors[key]
          };
        });
        //console.log(full_obj);

        var i,
          j = 0,
          k = 0,
          l = 0;
        var arr_colors = [];
        for (i = 0; i < imgData.data.length; i += 4) {

          // if (l == 0) {

          r = imgData.data[i].toString(16);
          g = imgData.data[i + 1].toString(16);
          b = imgData.data[i + 2].toString(16);

          if (r.length == 1) r = "0" + r;
          if (g.length == 1) g = "0" + g;
          if (b.length == 1) b = "0" + b;

          let currentrgb = r + g + b;
          let filtered = full_obj.filter(function (obj_val) {
            return obj_val.rgb === currentrgb;
          });
          let newhexcolor = filtered[0].nearest_color;
          //}

          /*if (k % newheight == 0) {
            imgData.data[i] = 0;
            imgData.data[i + 1] = 0;
            imgData.data[i + 2] = 0;
            imgData.data[i + 3] = 255;
          } else if (j % newwidth == 0) {
            imgData.data[i] = 0;
            imgData.data[i + 1] = 0;
            imgData.data[i + 2] = 0;
            imgData.data[i + 3] = 255;
          } else {
            imgData.data[i] = imgData.data[i];
            imgData.data[i + 1] = imgData.data[i + 1];
            imgData.data[i + 2] = imgData.data[i + 2];
            imgData.data[i + 3] = 255;
          }*/

          imgData.data[i] = hexToRgb(newhexcolor).r;
          imgData.data[i + 1] = hexToRgb(newhexcolor).g;
          imgData.data[i + 2] = hexToRgb(newhexcolor).b;
          imgData.data[i + 3] = 255;

          /*if ($.inArray(l, arr_final) != -1) {
            r = imgData.data[i].toString(16);
            g = imgData.data[i + 1].toString(16);
            b = imgData.data[i + 2].toString(16);

            if (r.length == 1) r = "0" + r;
            if (g.length == 1) g = "0" + g;
            if (b.length == 1) b = "0" + b;

            var finalcolor = "#" + r + g + b;
            arr_colors.push(finalcolor);            
          }*/

          if (i % 1160 == 0) {
            j = 0;
          }
          if (i % 1160 == 0) {
            k++;
          }
          j++;
          l++;
        }
        context.putImageData(imgData, 0, 0);
      }

      function dist(s, t) {
        s = s.substr(1);
        t = t.substr(1);
        if (!s.length || !t.length) return 0;
        return dist(s.slice(2), t.slice(2)) +
          Math.abs(parseInt(s.slice(0, 2), 16) - parseInt(t.slice(0, 2), 16));
      }

      function hexToRgb(hex) {
        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
          r: parseInt(result[1], 16),
          g: parseInt(result[2], 16),
          b: parseInt(result[3], 16)
        } : null;
      }

      pixelate(this);
    });

    return this;
  };
})(jQuery);
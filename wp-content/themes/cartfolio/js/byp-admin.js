jQuery(document).ready(function(){
    jQuery(".woocommerce_order_items_wrapper th.item.sortable").attr("colspan","1");
    
    jQuery("#menu-posts-pixelate .wp-menu-name").click(function(e) {
    	
    	e.preventDefault(); 
    });

    jQuery(document).on('click', '.byp-add-row', function() {

		cls_ele 	= jQuery(this).closest('.byp-group-button-sett');
		clone_ele	= jQuery(this).closest('.byp-group-btn-row').clone();

		// Retrieve the highest current key
		var key = highest = -1;
		cls_ele.find( 'tr.byp-group-btn-row' ).each(function() {
			var current = jQuery(this).data( 'key' );
			
			if( parseInt( current ) > highest ) {
				highest = current;
			}
		});
		key = highest += 1;

		clone_ele.attr( 'data-key', key );
		clone_ele.find( 'td input, td select, textarea' ).val( '' );

		clone_ele.find( 'input, select, textarea' ,'td').each(function() {
				var name = jQuery( this ).attr( 'name' );
				var id   = jQuery( this ).attr( 'id' );

				if( name ) {
					name = name.replace( /\[(\d+)\]/, '[' + parseInt( key ) + ']');
					jQuery( this ).attr( 'name', name );
				}

				jQuery( this ).attr( 'data-key', key );
				
				jQuery( this ).parents('tr').attr('id',key+'_byp_row');
				jQuery( this ).parents('tr').find('td .byp_visual_div').css('background-color', "");

				if( typeof id != 'undefined' ) {
					id = id.replace( /(\d+)/, parseInt( key ) );
					jQuery( this ).attr( 'id', id );
				}
			});
		clone_ele.appendTo('.byp-group-button-sett');// Clone and insert
		getColor();
	});

	// Delete row
	jQuery(document).on('click', '.byp-del-row', function() { 
		var msg 		= jQuery(this).attr('data-msg');
		var num_of_row 	= jQuery('.byp-group-button-sett .byp-group-btn-row').length;

		if(num_of_row == 1) {
			//alert(BwspAdmin.sry_msg);
			return false;
		} else {
			jQuery(this).closest('tr.byp-group-btn-row').remove();
		}
	});
	


});

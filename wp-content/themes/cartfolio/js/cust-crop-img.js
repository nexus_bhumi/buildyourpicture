jQuery(function() {  

  jQuery(document).on('change', '#file_input_1', function() {
    var input = jQuery(this),
    numFiles = input.get(0).files ? input.get(0).files.length : 1,
    file = input.get(0).files[0],
    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
    img_name =
     file.name;
    jQuery("#byp_add_order").attr('data-title', img_name);

    let reader = new FileReader()
    reader.readAsDataURL(file)     
    reader.onloadend = function() {   
      jQuery('#byp_ini_img').attr('src', reader.result);
      jQuery('#byp_prev_img').attr('src', reader.result); 
      jQuery("#byp_tmp_cropped").attr("src", reader.result);
      jQuery("#drop-area").hide();
      jQuery(".selective-section").find("*").prop("disabled", false);  
      jQuery(".selective-section").css("cursor", "pointer");
      jQuery(".crop_ratio_list, .radio-custom, .range, .transform-box button").removeClass('byp_no_click'); 
      byp_crop_img(50, 50,file);         
    } 
  });

  jQuery(".wonderplugin-gridgallery-list .wonderplugin-gridgallery-item-container").click(function(event) {    
    jQuery(".wonderplugin-gridgallery-list .wonderplugin-gridgallery-item-container").removeClass("byp-checked");
    jQuery("#byp_add_gimg").remove();
    var url = jQuery(this).find('a img').attr('src');     
    jQuery(this).addClass("byp-checked");
    jQuery(this).append('<a id="byp_add_gimg" data-url="'+url+'" href="#">Start</a>');

    jQuery("#byp_add_gimg").click(function(){

      var iurl = jQuery("#byp_add_gimg").attr("data-url");   
      var iname = jQuery("#byp_add_gimg").prev().attr("data-title");
      //console.log(iname);
      jQuery("#byp_add_order").attr('data-title', iname+'.jpg');
      jQuery( ".paoc-popup-close" ).click();
      convertFileToDataURLviaFileReader(iurl);
    });         
  });  

  jQuery(document).ready( function() {
    jQuery(':file').on('fileselect', function(event, numFiles, label) {
      var input = jQuery(this).parents('.input-group').find(':text'),
      log = numFiles > 1 ? numFiles + ' files selected' : label;
      jQuery("#byp_img_title").html(log);
    });  

    jQuery("#byp_start_ovr").click(function(){
      location.reload();
      jQuery("#drop-area").hide();
      jQuery("#byp_brightness").val(0);
      jQuery("#byp_contrast").val(0);
      jQuery("#byp_saturation").val(0);
      jQuery('input[name=color_gray_radio]:checked').val('fullcolor');      
    });

   /* window.onbeforeunload = function(event) {
      event.returnValue = "Data will be lost if you leave the page, are you sure?";
    };*/
  });
});

function convertFileToDataURLviaFileReader(url) {
  var xhr = new XMLHttpRequest();
  xhr.onload = function() {
    var reader = new FileReader();
    reader.onloadend = function() {        
      //console.log(reader.result);
    }
      
    reader.readAsDataURL(xhr.response);

    jQuery('#byp_ini_img').attr('src', url);
    jQuery('#byp_prev_img').attr('src',url);      
    jQuery("#byp_tmp_cropped").attr("src", url);

    if(jQuery(".cropper-crop-box")){
      jQuery(".cropper-crop-box img").attr("src",url);        
    }
    if(jQuery(".cropper-canvas")){
    	
      jQuery(".cropper-canvas img").attr("src",url);
      crop_btn = document.querySelector('#byp_apply_crop');
      crop_btn.click();
    }
   
    jQuery("#drop-area").hide();
    jQuery(".selective-section").find("*").prop("disabled", false);  
    jQuery(".selective-section").css("cursor", "pointer");
    jQuery(".crop_ratio_list, .radio-custom, .range, .transform-box button").removeClass('byp_no_click'); 
    byp_crop_img(50, 50, xhr.response); 

  };
  xhr.open('GET', url);
  xhr.responseType = 'blob';
  xhr.send();
}

/************************ Drag and drop *****************/
let dropArea = document.getElementById("drop-area")

// Prevent default drag behaviors
;['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
  dropArea.addEventListener(eventName, preventDefaults, false)   
  document.body.addEventListener(eventName, preventDefaults, false)
})

// Highlight drop area when item is dragged over it
;['dragenter', 'dragover'].forEach(eventName => {
  dropArea.addEventListener(eventName, highlight, false)
})

;['dragleave', 'drop'].forEach(eventName => {
  dropArea.addEventListener(eventName, unhighlight, false)
})

// Handle dropped files
dropArea.addEventListener('drop', handleDrop, false)

function preventDefaults (e) {
  e.preventDefault()
  e.stopPropagation()
}

function highlight(e) {
  dropArea.classList.add('highlight')
}

function unhighlight(e) {
  dropArea.classList.remove('active')
}

function handleDrop(e) {
  var dt = e.dataTransfer
  var files = dt.files
  handleFiles(files)    
}

function handleFiles(files) {

  files = [...files]  
  jQuery("#byp_img_title").html(files[0].name); 
  files.forEach(previewFile) 
}

function previewFile(file) {
  let reader = new FileReader()
  reader.readAsDataURL(file) 
  img_name = file.name;
  jQuery("#byp_add_order").attr('data-title', img_name);
 
  reader.onloadend = function() {   
    
    jQuery('#byp_ini_img').attr('src', reader.result);
    jQuery('#byp_prev_img').attr('src', reader.result); 
    jQuery("#byp_tmp_cropped").attr("src", reader.result);
    jQuery("#drop-area").hide();

    jQuery(".selective-section").find("*").prop("disabled", false);  
  jQuery(".selective-section").css("cursor", "pointer");
  jQuery(".crop_ratio_list, .radio-custom, .range, .transform-box button").removeClass('byp_no_click'); 
    byp_crop_img(50, 50,file);     
  } 
}
  

/***********************************************************************************************
  ************************** image crop functionality sratrs here ******************************/

function byp_crop_img(ratio_h, ratio_w, file) {

  jQuery("#byp_start_ovr").show();
  jQuery("#byp_action_btn").show();
  let cropper = '',
  crop_btn    = document.querySelector('#byp_apply_crop');   
  image1      = document.querySelector('#byp_ini_img'),
  byp_prev_img  = document.querySelector('#byp_prev_img')
  
  const reader = new FileReader();
  reader.readAsDataURL(file); 
  
  //// CHANGE START
  jQuery(".byp_data_ratio").click(function () {

    jQuery(".byp-overlay").show();    
    jQuery(".crop_ratio_list, .radio-custom, .range, .transform-box button").addClass('byp_no_click');
    jQuery(".loader").show();

    cropper.reset();
    ratio_h = jQuery(this).data("w");
    ratio_w = jQuery(this).data("h");
    jQuery("#crop_ratio_pixel").attr("min", ratio_h);
    jQuery("#crop_ratio_pixel").attr("max", ratio_w);

    if(ratio_w == '24' && ratio_h == '24'){
      jQuery("#byp_pro_price").html('35');
    } else if(ratio_w == '32' && ratio_h == '32'){
      jQuery("#byp_pro_price").html('50');
    } else if(ratio_w == '24' && ratio_h == '48'){
      jQuery("#byp_pro_price").html('55');
    } else if(ratio_w == '48' && ratio_h == '24'){
      jQuery("#byp_pro_price").html('55');
    } else {
      jQuery("#byp_pro_price").html('95');
    }

    hidden_ratio_w = jQuery("#crop_ratio_pixel").attr("min");
    hidden_ratio_h = jQuery("#crop_ratio_pixel").attr("max");
    var max_width = jQuery("#byp_max_width").val();
    cropper.setAspectRatio(hidden_ratio_w / hidden_ratio_h);
    

    if (hidden_ratio_w == hidden_ratio_h) {
      if (hidden_ratio_w < 50) {
        var newsize = 275 / 50;
        setTimeout(function () {
          cropper.setCropBoxData({
            width: newsize * hidden_ratio_w,
            height: newsize * hidden_ratio_w
          });
          crop_btn.click();
        }, 0);
      }

    }  
    crop_btn.click();

    in_val = jQuery("#byp_rotate").val();    
    if(in_val != ''){
      cropper.rotate(0);  
      cropper.rotate(in_val); 
      crop_btn.click();     
    }

    var byp_mirrorv = jQuery("#byp_mirrorv").val();
    if(byp_mirrorv != ''){
      cropper.scaleY(byp_mirrorv);
      crop_btn.click(); 
    }
    var byp_mirrorh = jQuery("#byp_mirrorh").val();
    if(byp_mirrorh != ''){
      cropper.scaleX(byp_mirrorh);
      crop_btn.click(); 
    }

  });  /*byp_data_ratio click END*/

  hidden_ratio_w = jQuery("#crop_ratio_pixel").attr("min");
  hidden_ratio_h = jQuery("#crop_ratio_pixel").attr("max");

  if((hidden_ratio_w == '') || (hidden_ratio_w == null) || (hidden_ratio_w == 'undefined')){
  
    ratio_h = hidden_ratio_h;
    ratio_w = hidden_ratio_w;

    cropper = new Cropper(image1 , {
      aspectRatio: ratio_h / ratio_w,
      viewMode: 1,                      
      movable: true,
      rotatable: true,
      dragMode: 'none',
      toggleDragModeOnDblclick: false,
      width: 500,
      height: 500,
      maxWidth: 4096,
   	  maxHeight: 4096,
      autoCropArea: croparea,
      responsive: true,
      restore: true,
      modal: true,
      cropBoxResizable: true,
      zoomable: false,
      
      cropend: function (e) {
         crop_btn.click(); 
      },
      crop: function (e) {     
        var data = e.detail;     
      }, 
                        
    });  

    crop_btn.click();
  
  } else {

    var croparea;
    if (ratio_w == '50' && ratio_h == '50') {

      croparea = '0.95' /// CHANGED
    } else if (ratio_w == '32' && ratio_h == '32') {
      croparea = '0.7'
    } else if (ratio_w == '24' && ratio_h == '24') {
      croparea = '0.5'
    } else {
      croparea = '0.95'; /// CHANGED
    }

    cropper = new Cropper(image1 , {
      aspectRatio: ratio_h / ratio_w,
      viewMode: 1,                      
      movable: true,
      rotatable: true,     
      dragMode: 'none',
      maxWidth: 4096,
      autoCropArea: croparea,
      toggleDragModeOnDblclick: false,
      maxHeight: 4096,
      responsive: true,
      restore: true,
      modal: true,
      cropBoxResizable: true,
      zoomable: false,
      cropstart: function (e) {           
      },
      cropmove: function (e) { 
      },
      cropend: function (e) {
         crop_btn.click(); 
         jQuery("#byp_prev_img").show();
         jQuery("#newCanvas0").remove();
      },
      crop: function (e) {     
        var data = e.detail;     
      }, 
                        
    });
  }
  
  setTimeout(function(){ 
    jQuery(".byp_data_ratio:first-child").click();     
     crop_btn.click();
     byp_make_pixalate();
  },50);

  var indrp = 1;
  var indrn = 1;



  jQuery("#clockwise").click(function () {
    
    in_val = jQuery("#byp_rotate").val();      
      if(in_val == ''){
        cropper.rotate(90);
        jQuery("#byp_rotate").val(90);
      }else {
        if(in_val == 270){          
          cropper.rotate(90);
          jQuery("#byp_rotate").val("");
        }else{
          val = 90+parseInt(in_val);
          cropper.rotate(90);
          jQuery("#byp_rotate").val(val);
        }
        
      }

    jQuery(".byp-overlay").show(); 
    jQuery(".selective-section").find("*").prop("disabled", true);  
    jQuery(".crop_ratio_list, .radio-custom, .range, .transform-box button").addClass('byp_no_click');
    jQuery(".loader").show();
    var old_cbox = cropper.getCropBoxData();
    var new_cbox = cropper.getCropBoxData();
    var old_canv = cropper.getCanvasData();
    var old_cont = cropper.getContainerData();
    var new_canv = cropper.getCanvasData();

    //calculate new height and width based on the container dimensions
    var heightOld = new_canv.height;
    var widthOld = new_canv.width;
    var heightNew = old_cont.height;
    var racio = heightNew / heightOld;
    var widthNew = new_canv.width * racio;
    new_canv.height = Math.round(heightNew);
    new_canv.width = Math.round(widthNew);
    newtop = 0;

    if (new_canv.width >= old_cont.width) {
      newleft = 0;
    } else {
      if (Math.round(heightNew) > Math.round(widthNew)) {
        newleft = 50;
      } else {
        newleft = Math.round((old_cont.width - new_canv.width) / 2);
      }
    }
   
    new_cbox_height = heightNew;
    new_cbox_width = widthNew;
    new_cbox_top = new_canv.top + racio * (old_cbox.left - old_canv.left);
    new_cbox_left = racio * (old_canv.height - old_cbox.height - old_cbox.top);

    new_cbox_height = Math.round(new_cbox_height);
    new_cbox_width = Math.round(new_cbox_width);
    new_cbox_top = Math.round(new_cbox_top);
    new_cbox_left = Math.round(new_cbox_left);
    setTimeout(function () {
      cropper.setCropBoxData({
        height: new_cbox_height,
        width: new_cbox_width,
        top: new_cbox_top,
        left: new_cbox_left
      });

      cropper.setCanvasData({
        height: Math.round(heightNew),
        width: Math.round(widthNew),
        top: newtop,
        left: newleft
      });

      crop_btn.click();
    }, 0); 
  });

  jQuery("#counterclockwise").click(function () {
   
      in_val = jQuery("#byp_rotate").val();
      
      if(in_val == ''){
        cropper.rotate(-90);
        jQuery("#byp_rotate").val(-90);
      }else {
        if(in_val == -270){
         
          cropper.rotate(-90);
          jQuery("#byp_rotate").val("");
        }else{
          val = -90+parseInt(in_val);
          cropper.rotate(-90);
          jQuery("#byp_rotate").val(val);
        }
        
      }      
      
    
    jQuery(".byp-overlay").show(); 
    jQuery(".selective-section").find("*").prop("disabled", true);
    jQuery(".crop_ratio_list, .radio-custom, .range, .transform-box button").addClass('byp_no_click');
    jQuery(".loader").show();
    var old_cbox = cropper.getCropBoxData();
    var new_cbox = cropper.getCropBoxData();
    var old_canv = cropper.getCanvasData();
    var old_cont = cropper.getContainerData();
    var new_canv = cropper.getCanvasData();
    //calculate new height and width based on the container dimensions
    var heightOld = new_canv.height;
    var widthOld = new_canv.width;
    var heightNew = old_cont.height;
    var racio = heightNew / heightOld;
    var widthNew = new_canv.width * racio;
    new_canv.height = Math.round(heightNew);
    new_canv.width = Math.round(widthNew);
    newtop = 0;

    if (new_canv.width >= old_cont.width) {
      newleft = 0;
    } else {
      newleft = Math.round((old_cont.width - new_canv.width) / 2);
    }

    new_cbox_height = heightNew;
    new_cbox_width = widthNew;
    new_cbox_top = new_canv.top + racio * (old_cbox.left - old_canv.left);
    new_cbox_left = racio * (old_canv.height - old_cbox.height - old_cbox.top);
    new_cbox_height = Math.round(new_cbox_height);
    new_cbox_width = Math.round(new_cbox_width);
    new_cbox_top = Math.round(new_cbox_top);
    new_cbox_left = Math.round(new_cbox_left);
    setTimeout(function () {
      cropper.setCropBoxData({
        height: new_cbox_height,
        width: new_cbox_width,
        top: new_cbox_top,
        left: new_cbox_left
      });

      cropper.setCanvasData({
        height: Math.round(heightNew),
        width: Math.round(widthNew),
        top: newtop,
        left: newleft
      });

      crop_btn.click();
    }, 0);
  });

  jQuery( "#mirrorv" ).toggle(function() {

    jQuery(".byp-overlay").show(); 
    jQuery(".selective-section").find("*").prop("disabled", true);  
    
    jQuery(".crop_ratio_list, .radio-custom, .range, .transform-box button").addClass('byp_no_click');
    jQuery(".loader").show();
    cropper.scaleY(-1);
    jQuery("#byp_mirrorv").val('-1');
    crop_btn.click(); 

  }, function() {
    cropper.scaleY(1);
    jQuery("#byp_mirrorv").val('1');
    crop_btn.click(); 
  });

  jQuery( "#mirrorh" ).toggle(function() {
    jQuery(".byp-overlay").show(); 
    jQuery(".selective-section").find("*").prop("disabled", true); 
  
    jQuery(".crop_ratio_list, .radio-custom, .range, .transform-box button").addClass('byp_no_click');
    jQuery(".loader").show();
    cropper.scaleX(-1);
    jQuery("#byp_mirrorh").val('-1');
    crop_btn.click(); 
  }, function() {
    cropper.scaleX(1);
    jQuery("#byp_mirrorh").val('1');
    crop_btn.click(); 
  });

  jQuery("input[type='radio']").on('change', function () {
    var byp_clr_gry = jQuery('input[name=color_gray_radio]:checked').val();
    jQuery(".byp-overlay").show(); 
    jQuery(".selective-section").find("*").prop("disabled", true);   
    jQuery(".crop_ratio_list, .radio-custom, .range, .transform-box button").addClass('byp_no_click');
    jQuery(".loader").show();
    if(byp_clr_gry == 'gray'){
      byp_apply_filter(true);
    } else {
      byp_apply_filter();
    }  
  });  
   
  jQuery("#crop_ratio_pixel").attr("min",ratio_w );  
  jQuery("#crop_ratio_pixel").attr("max", ratio_h); 

   jQuery("input[type='range']").on("input", function() {
    var filter_id = jQuery(this).attr("ID");
    var bri_val = jQuery("#byp_brightness").val();
    jQuery("#brightness").html(bri_val);        
  
    var cont_val = jQuery("#byp_contrast").val();
    jQuery("#contrest").html(cont_val);    
  
    var sat_val = jQuery("#byp_saturation").val();
    jQuery("#saturation").html(sat_val);  
    
  }); 

  jQuery("input[type='range']").change(function(){    
    
    setTimeout(function(){ 
      jQuery(".loader").show();
      jQuery(".byp-overlay").show(); 
      jQuery(".selective-section").find("*").prop("disabled", true);  
      jQuery(".crop_ratio_list, .radio-custom, .range, .transform-box button").addClass('byp_no_click');
      byp_apply_filter();
    },2);     
      
  });
  
  crop_btn.addEventListener('click',(e) => {

    e.preventDefault();   
   	
    img_w = jQuery(".cropper-face").width();
    img_h = jQuery(".cropper-face").height();   
    
    cropped = document.getElementById("byp_prev_img");
    cropped.width = 400;
    cropped.height = 410;

    let imgSrc = cropper.getCroppedCanvas({
      width: img_w // input value
    }).toDataURL();    

    cropped.src = imgSrc;
   
    jQuery("#byp_tmp_cropped").attr("src", imgSrc);    

    /*********************************** filter options **********************************/
  	

    jQuery("input[type='range']").on("input", function() { 

      var filter_id = jQuery(this).attr("ID");
      var bri_val = jQuery("#byp_brightness").val();
      jQuery("#brightness").html(bri_val);
      var cont_val = jQuery("#byp_contrast").val();
      jQuery("#contrest").html(cont_val);
      var sat_val = jQuery("#byp_saturation").val();
      jQuery("#saturation").html(sat_val);  
      
    });

    jQuery("input[type='range']").change(function(){  
    
      setTimeout(function(){ 
      jQuery(".loader").show();
      jQuery(".byp-overlay").show(); 
      jQuery(".selective-section").find("*").prop("disabled", true);  
      jQuery(".crop_ratio_list, .radio-custom, .range, .transform-box button").addClass('byp_no_click');
      byp_apply_filter();
      },2); 
   
    });  
  
    jQuery('input[name=color_gray_radio]').click(function() {

      jQuery('input[name=color_gray_radio]').prop('checked');

      
      var byp_clr_gry = jQuery('input[name=color_gray_radio]:checked').val();
      
      if(byp_clr_gry == 'gray'){
        byp_apply_filter(true);
      } else {
        byp_apply_filter();
      }
    }); 
    /*********************************** end filter options **********************************/ 

    setTimeout(function(){ 
     byp_apply_filter();

     
    },0);

  }); 
}

function byp_rotate(){
  
}

function byp_make_pixalate() { 
 
  var byp_clr_gry = jQuery('input[name=color_gray_radio]:checked').val();

 var data = {
    action:       'byp_get_pixelate_optns',
    filter_clr:    byp_clr_gry,
    is_ajax:       1,
  };

  jQuery.post(cci.ajaxurl, data, function (response) {  
    if(response){
     // json_decode(response);
      var pixelate_properties = jQuery.parseJSON(response).pixelate_properties;
      var pix_border_prop = jQuery.parseJSON(response).pix_border_prop;
    }
    
    
     setTimeout(function(){      
      var minpix = jQuery("#crop_ratio_pixel").attr("min");  
      var maxpix = jQuery("#crop_ratio_pixel").attr("max");

      jQuery("#byp_prev_img").pixelate({                   
        'focus1' : minpix,
        'focus2' : maxpix, 
        'pixelate_properties' : pixelate_properties,                 
        'pix_border_prop' : pix_border_prop,                 
      });
    },100);

  });
   
}

function byp_apply_filter(grayscale){
  
  var ind = 100; 
  var bri_val = jQuery("#byp_brightness").val();
  var cont_val = jQuery("#byp_contrast").val();
  var sat_val = jQuery("#byp_saturation").val();
  
  if(bri_val >= 0) {
    var br = (isNaN(bri_val) || (bri_val == 0) ) ?  '100' : parseFloat(bri_val) +parseFloat(100);   
    var bri_ind = (isNaN(bri_val) || (bri_val == 0) ) ?  '1' : parseFloat(bri_val)/100 +parseFloat(1);    
  } else {
    var br = (isNaN(bri_val) || (bri_val == 0) ) ? '100' : parseFloat(bri_val) + parseFloat(100) ;   
    var bri_ind = (isNaN(bri_val) || (bri_val == 0) ) ? '0.9' : (parseFloat(bri_val) + parseFloat(101) ) / 100;    
  }

  if(cont_val >= 0) {
    var cont_ind = isNaN(cont_val) ? 100 : parseFloat(cont_val) + parseFloat(ind);
    var sr = (isNaN(cont_val) || (cont_val == 0) ) ?  '100' : parseFloat(cont_val) +parseFloat(100);
  } else {
    var cont_ind = isNaN(cont_val) ? 100 : parseFloat(cont_val) + parseFloat(ind);
    var sr = (isNaN(cont_val) || (cont_val == 0) ) ?  '100' : parseFloat(cont_val) +parseFloat(100);
  }

  if(sat_val >= 0) {
    var satu_ind = (isNaN(sat_val) || (sat_val == 0) ) ? 100 : parseFloat(sat_val) + parseFloat(110);
    var sr = (isNaN(sat_val) || (sat_val == 0) ) ?  '100' : parseFloat(sat_val) +parseFloat(110);
  } else {
    var satu_ind = (isNaN(sat_val) || (sat_val == 0) ) ? 100 : parseFloat(sat_val) + parseFloat(120);
    var sr = (isNaN(sat_val) || (sat_val == 0) ) ?  '100' : parseFloat(sat_val) +parseFloat(110);
  }

  setTimeout(function(){ 
    ori_i_src = jQuery("#byp_tmp_cropped").attr("src");      
    cropped = document.getElementById("byp_prev_img");
    jQuery("#byp_prev_img").attr("src", ori_i_src);

    /// CHANGE START
    var preview_height = jQuery("#byp_tmp_cropped").height();
    var preview_width = jQuery("#byp_tmp_cropped").width();
    var container_height = jQuery(".process-output-image").height() + 4;
    var container_width = jQuery(".process-output-image").width() + 4;

    var newheight = 0;
    var newwidth = 0;

    if (preview_height == preview_width) {
      newheight = container_height;
      newwidth = container_height;
    } else if (preview_height > preview_width) {
      newheight = container_height;
      newwidth = (preview_width * container_height) / preview_height;
    } else if (preview_height < preview_width) {
      newwidth = container_width;
      newheight = (preview_height * container_width) / preview_width;
    }

    jQuery("#byp_prev_img").css("height", newheight);
    jQuery("#byp_prev_img").css("width", newwidth);

    /// CHANGE END
    var canvas = document.getElementById('byp_temp_canvas');
    var fimg1 = document.querySelector('.cropper-canvas img');
    var fimg2 = document.querySelector('.cropper-crop-box');
    canvas.width = cropped.width;
    canvas.height = cropped.height;
    var ctx = canvas.getContext('2d');  

    var byp_clr_gry = jQuery('input[name=color_gray_radio]:checked').val();
    
    if(grayscale == true || (byp_clr_gry == "gray")) {
      ctx.filter = 'contrast('+cont_ind+'%) saturate('+satu_ind+'%) brightness('+bri_ind+') grayscale(100%)';
      
      jQuery('.cropper-canvas img').css(
        "filter", 'brightness(' + br +
        '%) contrast(' + cont_ind +
        '%)  saturate(' + sr +
        '%) grayscale(100%)'
      );
      jQuery('.cropper-crop-box').css(
        "filter", 'brightness(' + br +
        '%) contrast(' + cont_ind +
        '%)  saturate(' + sr +
        '%) grayscale(100%)'
      );

    } else {
      ctx.filter = 'contrast('+cont_ind+'%) saturate('+satu_ind+'%) brightness('+bri_ind+')';

      jQuery('.cropper-canvas img').css(
        "filter", 'brightness(' + br +
        '%) contrast(' + cont_ind +
        '%)  saturate(' + sr +
        '%) '
      );
      jQuery('.cropper-crop-box').css(
        "filter", 'brightness(' + br +
        '%) contrast(' + cont_ind +
        '%)  saturate(' + sr +
        '%) '
      );
   }          

    ctx.drawImage(cropped,0,0,cropped.width,cropped.height);
    jQuery("#byp_temp_canvas").show();

    var can = document.getElementById("byp_temp_canvas");
    jQuery("#byp_prev_img").attr("src",can.toDataURL());
    jQuery("#byp_temp_canvas").hide();    
  },10);

  setTimeout(function(){  
    byp_make_pixalate(); 
  },15); 
}

/******************************** ADD TO ORDER SCRIPT ******************************/ 
jQuery("#byp_add_order").click(function(){

  var byp_qty = jQuery("#byp_qty").val();
  var canvas        = document.getElementById("newCanvas0");
  var mosaic_img    = canvas.toDataURL("image/png");
  var original_img  = jQuery("#byp_ini_img").attr("src");
  var cropped_img   = jQuery("#byp_tmp_cropped").attr("src");
  var img_name      = jQuery("#byp_add_order").attr('data-title'); 
  var img_price     = jQuery("#byp_pro_price") .html();    
  var minpix        = jQuery("#crop_ratio_pixel").attr("min");  
  var maxpix        = jQuery("#crop_ratio_pixel").attr("max");
  var product_id    = jQuery("#byp_edit_cimg").val();
  var byp_csv_data    = jQuery("#byp_csv_data").attr('data-json');
  
  if(product_id){product_id = product_id; } else {product_id = "";}

  var data = {
    action:         'byp_add_order',
    mosaic_img:     mosaic_img,
    original_img:   original_img,
    cropped_img:    cropped_img,
    img_name:       img_name, 
    minpix:         minpix, 
    maxpix:         maxpix, 
    product_id:     product_id,
    img_price:      img_price,
    byp_csv_data:   byp_csv_data,
    is_ajax:        1
  };

  jQuery.post(cci.ajaxurl, data, function (response) {

    console.log(response);
    jQuery(".addtocartv").attr('value', response);   

    if(product_id){

      if(byp_qty > 1){  jQuery(".custom_add_to_cart").click(); }
      jQuery(".loader-addtocart").hide();
      localStorage.removeItem("byp_edit_cimg");
      localStorage.removeItem("byp_edit_curl");
      jQuery("#byp_edit_cimg").val("");
      jQuery("#byp_add_order").html("Voeg toe aan winkelmand");
      jQuery("#byp_prod_added").html("het product is met succes aangepast in je winkelmand!"); 
      jQuery("#bypmodaladdcartclik").show();

      /* refresh fragments*/
       var cart_hash_key = substitutions.cart_hash_key;

    function refresh_fragments() {
        // Better to put there 'blank' string then empty string
        window.sessionStorage.setItem(cart_hash_key, 'blank'); 
        var e = $.Event("storage");

        e.originalEvent = {
            key: cart_hash_key,
        };

        $(window).trigger(e);
    }

    refresh_fragments();
    setInterval(refresh_fragments, 60000);
      /* refresh fragments*/

      $('#byp_succ_cart').html("cart has been updated.").fadeIn('slow');
      $('#byp_succ_cart').delay(2000).fadeOut("cart has been updated."); 
      jQuery("#byp_add_order").html("Voeg toe aan winkelmand"); 
                

    } else{
      setTimeout(function(){
        jQuery("#byp_prod_added").html("Hoppa, het product zit in je winkelmand!");
        jQuery(".custom_add_to_cart").click();
        jQuery("#byp_add_order").html("Voeg toe aan winkelmand");

      }, 1); 
    }
            
  });
   jQuery(".loader-addtocart").show();
});

//Add to cart script header cart update
jQuery(document).ready(function ($) {
  "use strict";

  var pro_id = localStorage.getItem("byp_edit_cimg");
  var pro_url = localStorage.getItem("byp_edit_curl");
  if(pro_id){
    $("#byp_edit_cimg").val(pro_id);
    jQuery("body").trigger("wc_fragments_refreshed");
    jQuery("#byp_add_order").html("Winkelmand bijwerken");
    convertFileToDataURLviaFileReader(pro_url);
  }    

  $('.custom_add_to_cart').click(function (e) {

    e.preventDefault();
    var byp_qty = $("#byp_qty").val();
    var id = $(this).next().next().attr('value');
    var data = {
      product_id: id,
      quantity: byp_qty
    };
    $(this).parent().addClass('loading');
    $.post(wc_add_to_cart_params.wc_ajax_url.toString().replace('%%endpoint%%', 'add_to_cart'), data, function (response) {

      if (!response) {
        return;
      }
      if (response.error) {        
        return;
      }
      if (response) {

        var url = woocommerce_params.wc_ajax_url;
        url = url.replace("%%endpoint%%", "get_refreshed_fragments");
        $.post(url, function (data, status) {
          $(".woocommerce.widget_shopping_cart").html(data.fragments["div.widget_shopping_cart_content"]);
          if (data.fragments) {
            $.each(data.fragments, function (key, value) {
              $(key).replaceWith(value);
            });
          }
          $("body").trigger("wc_fragments_refreshed");
        });
        $(".loader-addtocart").hide();
        $("#bypmodaladdcartclik").show();            
      }
    });
 });  
});

jQuery(document).ready(function(){
   jQuery("#bypmodaladdcartclik").hide();
   jQuery(".close").click(function(){
  jQuery("#bypmodaladdcartclik").hide();
  });
 jQuery(".editimgbtn").click(function(){
    jQuery(".close").click();
  });
});
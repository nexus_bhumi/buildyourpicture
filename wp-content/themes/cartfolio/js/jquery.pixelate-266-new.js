/*!

Name: highlightReel
Dependencies: jQuery
Author: Justin Duke
Author URL: http://jmduke.com
Github URL: https://github.com/jmduke
Licensed under the MIT license
*/

(function($) {
  $.fn.pixelate = function(options) {
    // TODO:  There's, like, definitely more customization that we could do here.
    var defaults = {
        focus: 1,
        canvasID: "newCanvas"
      },
      settings = $.extend({}, defaults, options);

       /* get all colors array */
      var pixelate_properties = settings.pixelate_properties;
     /* var pixelate_prop_arr = [];
      var pixelate_properties = pixelate_properties.split(',');
      for (var i = 0; i < pixelate_properties.length; i++) {
        pixelate_prop_arr.push(parseInt(pixelate_properties[i]));       
      }*/

      /* color array: pixelate_prop_arr */

    this.each(function(index) {
      // Since we pretty much remove the <img> element from the picture pretty quickly, we need to
      // capture the important attributes before anything else.
      var preview_height = jQuery("#byp_tmp_cropped").height();
      var preview_width = jQuery("#byp_tmp_cropped").width();
      var container_height = jQuery(".process-output-image").height() + 4;
      var container_width = jQuery(".process-output-image").width() + 4;

      var newheight = 0;
      var newwidth = 0;

      if (preview_height == preview_width) {
        newheight = container_height;
        newwidth = container_height;
      } else if (preview_height > preview_width) {
        newheight = container_height;
        newwidth = (preview_width * container_height) / preview_height;
      } else if (preview_height < preview_width) {
        newwidth = container_width;
        newheight = (preview_height * container_width) / preview_width;
      }

      var width = newwidth;
      var height = newheight;

      /*var width = this.width;
      var height = this.height;*/

      var idSelector = "#" + settings.canvasID + index;

      // Hide the image itself, since we're transposing over it.
      $(this).hide();

      // Edge case: in multiple invocations of pixelate(), be sure to get rid of previous attempts.
      $(this)
        .parent()
        .children("canvas")
        .hide();
      $("#newCanvas0").remove();
      $(this).after("<canvas id='" + idSelector.slice(1) + "'>");

      $(idSelector).get(0).width = width;
      $(idSelector).get(0).height = height;

      var el = $("#" + settings.canvasID + index).get(0);

      var relativeWidth = settings.focus1;
      var relativeHeight = settings.focus2;

      if (width == height) {
        if (relativeWidth == 50 && relativeHeight == 50) {
          $(idSelector).get(0).width = width + 10;
          $(idSelector).get(0).height = height + 10;
        } else {
          $(idSelector).get(0).width = width - 2;
          $(idSelector).get(0).height = height - 2;
        }
      } else if (height == 290) {
        $(idSelector).get(0).height = height - 2;
        $(idSelector).get(0).width = width - 2;
      }

      var context = $(idSelector)
        .get(0)
        .getContext("2d");

      // This is how the whole thing works.
      context.imageSmoothingEnabled = false;
      context.mozImageSmoothingEnabled = false;
      context.webkitImageSmoothingEnabled = false;

      function pixelate(image) {
        $(".loader").show();
        var el = $("#" + settings.canvasID + index).get(0);

        var relativeWidth = settings.focus1;
        var relativeHeight = settings.focus2;

        context.drawImage(image, 0, 0, relativeWidth, relativeHeight);
        context.drawImage(
          el,
          0,
          0,
          relativeWidth,
          relativeHeight,
          0,
          0,
          el.width,
          el.height
        );

        var imgData = context.getImageData(0, 0, el.width, el.height);
        var newwidth = Math.round(imgData.width / relativeWidth);
        var newheight = Math.round(imgData.height / relativeHeight);

        var halfwidth = Math.round(newwidth / 2);
        var halfheight = Math.round(newheight / 2);

        var arr_x = [];
        var x_width = 0;
        for (x = 1; x <= relativeWidth; x++) {
          if (x == 1) {
            x_width = x_width + halfwidth;
          } else {
            x_width = x_width + newwidth;
          }
          arr_x.push(x_width);
        }

        var arr_y = [];
        var y_height = 0;
        for (y = 1; y <= relativeHeight; y++) {
          if (y == 1) {
            y_height = y_height + halfheight;
          } else {
            y_height = y_height + newheight;
          }
          arr_y.push(y_height);
        }

        if (imgData.width > imgData.height) {
          var perlinepixel = imgData.width;
        } else if (imgData.width < imgData.height) {
          var perlinepixel = imgData.width;
        } else {
          var perlinepixel = imgData.width;
        }

        var arr_final = [];
        $.each(arr_y, function(index1, value1) {
          $.each(arr_x, function(index2, value2) {
            var position_y =
              parseInt(perlinepixel) * parseInt(value1) + parseInt(value2);
            arr_final.push(position_y);
          });
        });

        var count = 0;
        var newcount = 0;
        var unique_colors = [];
        var dict = {};
        for (i = 0; i < imgData.data.length; i += 4) {
          if ($.inArray(count, arr_final) != -1) {
            r = imgData.data[i].toString(16);
            g = imgData.data[i + 1].toString(16);
            b = imgData.data[i + 2].toString(16);

            if (r.length == 1) r = "0" + r;
            if (g.length == 1) g = "0" + g;
            if (b.length == 1) b = "0" + b;

            var finalcolor = "#" + r + g + b;
            unique_colors.push(finalcolor);

            var finalrgb = r + g + b;
            dict[newcount] = {
              rgb: finalrgb,
              color: finalcolor,
              curr_r: imgData.data[i],
              curr_g: imgData.data[i + 1],
              curr_b: imgData.data[i + 2]
            };
            newcount++;
          }
          count++;
        }

        var color_option = $("input[name='color_gray_radio']:checked").val();
        if (color_option == "gray") {
          var all_colors_rgb = [
            250,
            250,
            250,
            172,
            172,
            172,
            55,
            55,
            55,
            05,
            05,
            05
          ];
        } else {
          var all_colors_rgb = [
            16,
            120,
            32,
            251,
            94,
            22,
            247,
            205,
            02,
            126,
            155,
            22,
            235,
            11,
            11,
            127,
            18,
            19,
            252,
            175,
            227,
            43,
            25,
            84,
            09,
            58,
            132,
            00,
            146,
            215,
            96,
            175,
            216,
            243,
            213,
            135,
            231,
            185,
            97,
            68,
            32,
            19,
            179,
            110,
            46,
            250,
            250,
            250,
            172,
            172,
            172,
            55,
            55,
            55,
            05,
            05,
            05
          ];
        }

        var all_colors_hex = [];

        for (i = 0; i < all_colors_rgb.length; i += 3) {
          r = all_colors_rgb[i].toString(16);
          g = all_colors_rgb[i + 1].toString(16);
          b = all_colors_rgb[i + 2].toString(16);

          if (r.length == 1) r = "0" + r;
          if (g.length == 1) g = "0" + g;
          if (b.length == 1) b = "0" + b;

          var finalcolor = "#" + r + g + b;
          all_colors_hex.push(finalcolor);
        }

        var full_obj = [];
        var colorarray = [];
        $.map(dict, function(val, key) {
          var closest = 0;
          var closest_r, closest_g, closest_b;

          let img_r = hexToRgb(val.color).r;
          let img_g = hexToRgb(val.color).g;
          let img_b = hexToRgb(val.color).b;

          var j = 0;
          for (s = 0; s < all_colors_rgb.length; s += 3) {
            j++;
            rr = all_colors_rgb[s];
            gg = all_colors_rgb[s + 1];
            bb = all_colors_rgb[s + 2];

            if (rr.length == 1) rr = "0" + rr;
            if (gg.length == 1) gg = "0" + gg;
            if (bb.length == 1) bb = "0" + bb;

            var finalresult = colorDifference(img_r, img_g, img_b, rr, gg, bb);
            if (s == 0) {
              closest = finalresult;
              closest_r = rr;
              closest_g = gg;
              closest_b = bb;
            } else {
              if (finalresult < closest) {
                closest = finalresult;
                closest_r = rr;
                closest_g = gg;
                closest_b = bb;
              }
            }
          }

          colorarray.push(img_r);
          colorarray.push(img_g);
          colorarray.push(img_b);

          full_obj[key] = {
            rgb: val.rgb,
            color: val.color,
            closest_r: closest_r,
            closest_g: closest_g,
            closest_b: closest_b
          };
        });

        var c = document.getElementById("byp_temp_canvas_temp");
        var ctx = c.getContext("2d");

        var imgData_new = ctx.createImageData(relativeWidth, relativeHeight);
        var i,
          k = 0;

        for (i = 0; i < imgData_new.data.length; i += 4) {
          imgData_new.data[i + 0] = colorarray[k];
          imgData_new.data[i + 1] = colorarray[k + 1];
          imgData_new.data[i + 2] = colorarray[k + 2];
          imgData_new.data[i + 3] = 255;
          k += 3;
        }

        for (var y = 0; y < imgData_new.height; y++) {
          for (var x = 0; x < imgData_new.width; x++) {
            var clr = getColorAtindex(imgData_new, x, y);

            var oldR = clr.red;
            var oldG = clr.green;
            var oldB = clr.blue;

            var closest = 0;
            var closest_r, closest_g, closest_b;
            var s, rr, gg, bb;
            for (s = 0; s < all_colors_rgb.length; s += 3) {
              rr = all_colors_rgb[s];
              gg = all_colors_rgb[s + 1];
              bb = all_colors_rgb[s + 2];

              if (rr.length == 1) rr = "0" + rr;
              if (gg.length == 1) gg = "0" + gg;
              if (bb.length == 1) bb = "0" + bb;

              var finalresult = colorDifference(oldR, oldG, oldB, rr, gg, bb);
              if (s == 0) {
                closest = finalresult;
                closest_r = rr;
                closest_g = gg;
                closest_b = bb;
              } else {
                if (finalresult < closest) {
                  closest = finalresult;
                  closest_r = rr;
                  closest_g = gg;
                  closest_b = bb;
                }
              }
            }

            var newClr = {
              red: closest_r,
              green: closest_g,
              blue: closest_b
            };

            setColorAtIndex(imgData_new, x, y, newClr);

            var errR = oldR - closest_r;
            var errG = oldG - closest_g;
            var errB = oldB - closest_b;

            distributeError(imgData_new, x, y, errR, errG, errB);
          }
        }

        ctx.putImageData(imgData_new, 0, 0);


        var k=0;
    var count = 0, count2 = 0, count3 = 0, count4 = 0, count5 = 0, count6 = 0, count7 = 0, count8 = 0, 
    count9 = 0, count10 = 0, count11 = 0, count12 = 0, count13 = 0, count14 = 0, count15 = 0, count16 = 0;
    var global_data = [], completed_scan = [];
    var lastprocess = "";
    var color_ojb = [
        {
            r:16,
            g:120,
            b:32,
            color: 'green'
        },
        {
            r:251,
            g:94,
            b:22,
            color: 'orange'
        },
        {
            r:247,
            g:205,
            b:2,
            color: 'yellow'
        },
        {
            r:126,
            g:155,
            b:22,
            color: 'lime green'
        },
        {
            r:235,
            g:11,
            b:11,
            color: 'red'
        },
        {
            r:127,
            g:18,
            b:19,
            color: 'dark red'
        },
        {
            r:252,
            g:175,
            b:227,
            color: 'pink'
        },
        {
            r:43,
            g:25,
            b:84,
            color: 'purple'
        },
        {
            r:9,
            g:58,
            b:132,
            color: 'blue'
        },
        {
            r:0,
            g:146,
            b:215,
            color: 'light blue'
        },
        {
            r:96,
            g:175,
            b:216,
            color: 'baby blue'
        },
        {
            r:243,
            g:213,
            b:135,
            color: 'tan'
        },
        {
            r:231,
            g:185,
            b:97,
            color: 'dark tan'
        },
        {
            r:68,
            g:32,
            b:19,
            color: 'brown'
        },
        {
            r:179,
            g:110,
            b:46,
            color: 'nougat'
        },
        {
            r:250,
            g:250,
            b:250,
            color: 'white'
        },
        {
            r:172,
            g:172,
            b:172,
            color: 'light grey'
        },
        {
            r:55,
            g:55,
            b:55,
            color: 'dark grey'
        },
        {
            r:5,
            g:5,
            b:5,
            color: 'black'
        },
    ];
    for (var y = 0; y < imgData_new.height; y++) {
        for (var x = 0; x < imgData_new.width; x++) {
            //var currentscan = x+','+y;
            var isprocessed = "";
            var clr = getColorAtindex(imgData_new, x, y);      
            var curR = clr.red;
            var curG = clr.green;
            var curB = clr.blue;         
            
            var filtered = color_ojb.filter(function(obj_val) {
                return obj_val.r === curR && obj_val.g === curG && obj_val.b === curB;
            });
            
            var colorname = filtered[0].color;
            
            var mainIndex = imageIndex(imgData_new, x, y);
            
            if (jQuery.inArray(mainIndex, completed_scan) != -1) {
                isprocessed = "yes";
            }
            
            //console.log(x, y, curR, curG, curB);
            
            // 2x8 Horizontal check for each pixel
            if(imgData_new.width-7 >= x && imgData_new.height-1 >= y && lastprocess!="1" && isprocessed == ""){
                var notmatch = "";
                var tempIndexes = [];
                for(var newy=y; newy <= y+1; newy++){
                    for(var newx=x; newx<=x+7; newx++){                        
                        if(newx!=x || newy!=y){                            
                            var currentIndex = imageIndex(imgData_new, newx, newy);
                            tempIndexes.push(currentIndex);
                            var find_clr = getColorAtindex(imgData_new, newx, newy);
                            var getR = find_clr.red;
                            var getG = find_clr.green;
                            var getB = find_clr.blue;
                            
                            if (jQuery.inArray(currentIndex, completed_scan) != -1) {
                                notmatch = "yes";
                            }
                            
                            if(curR!=getR || curG!=getG || curB!=getB){
                                notmatch = "yes";
                            }
                        }                        
                    }                    
                }
                
                if(notmatch == ''){
                    isprocessed = "yes";
                    lastprocess = "1";
                    completed_scan.push(mainIndex);
                    tempIndexes.forEach(function(index){
                        if (jQuery.inArray(index, completed_scan) == -1) {
                            completed_scan.push(index);
                        }                        
                    });
                    count++;
                    common_logic(global_data, k, x+1, y+1, "2x8 horizontal", curR, curG, curB, colorname,8,2);
                }
            }
            
            // 2x8 Vertical check for each pixel
            if(imgData_new.height-7 >= y && imgData_new.width-1 >= x && lastprocess!="2" && isprocessed == ""){
                var notmatch = "";
                var tempIndexes = [];
                for(var newx=x; newx <= x+1; newx++){
                    for(var newy=y; newy<=y+7; newy++){                        
                        if(newx!=x || newy!=y){                            
                            var currentIndex = imageIndex(imgData_new, newx, newy);
                            tempIndexes.push(currentIndex);
                            var find_clr = getColorAtindex(imgData_new, newx, newy);
                            var getR = find_clr.red;
                            var getG = find_clr.green;
                            var getB = find_clr.blue;
                            
                            if (jQuery.inArray(currentIndex, completed_scan) != -1) {
                                notmatch = "yes";
                            }
                            
                            if(curR!=getR || curG!=getG || curB!=getB){
                                notmatch = "yes";
                            }
                        }                        
                    }                    
                }
                
                if(notmatch == ''){
                    isprocessed = "yes";
                    lastprocess = "2";
                    completed_scan.push(mainIndex);
                    tempIndexes.forEach(function(index){
                        if (jQuery.inArray(index, completed_scan) == -1) {
                            completed_scan.push(index);
                        }                        
                    });
                    
                    count2++;
                    common_logic(global_data, k, x+1, y+1, "2x8 vertical", curR, curG, curB, colorname,2,8);
                }
            }
            
            // 2x6 Horizontal check for each pixel
            if(imgData_new.width-5 >= x && imgData_new.height-1 >= y && lastprocess!="3" && isprocessed == ""){
                var notmatch = "";
                var tempIndexes = [];
                for(var newy=y; newy <= y+1; newy++){
                    for(var newx=x; newx<=x+5; newx++){                        
                        if(newx!=x || newy!=y){                            
                            var currentIndex = imageIndex(imgData_new, newx, newy);
                            tempIndexes.push(currentIndex);
                            var find_clr = getColorAtindex(imgData_new, newx, newy);
                            var getR = find_clr.red;
                            var getG = find_clr.green;
                            var getB = find_clr.blue;
                            
                            if (jQuery.inArray(currentIndex, completed_scan) != -1) {
                                notmatch = "yes";
                            }
                            
                            if(curR!=getR || curG!=getG || curB!=getB){
                                notmatch = "yes";
                            }
                        }                        
                    }                    
                }                
                if(notmatch == ''){
                    isprocessed = "yes";
                    lastprocess = "3";
                    completed_scan.push(mainIndex);
                    tempIndexes.forEach(function(index){
                        if (jQuery.inArray(index, completed_scan) == -1) {
                            completed_scan.push(index);
                        }                        
                    });
                    count3++;
                    common_logic(global_data, k, x+1, y+1, "2x6 horizontal", curR, curG, curB, colorname,6,2);
                }
            }
            
            // 2x6 Vertical check for each pixel
            if(imgData_new.height-5 >= y && imgData_new.width-1 >= x && lastprocess!="4" && isprocessed == ""){
                var notmatch = "";
                var tempIndexes = [];
                for(var newx=x; newx <= x+1; newx++){
                    for(var newy=y; newy<=y+5; newy++){                        
                        if(newx!=x || newy!=y){                            
                            var currentIndex = imageIndex(imgData_new, newx, newy);
                            tempIndexes.push(currentIndex);
                            var find_clr = getColorAtindex(imgData_new, newx, newy);
                            var getR = find_clr.red;
                            var getG = find_clr.green;
                            var getB = find_clr.blue;
                            
                            if (jQuery.inArray(currentIndex, completed_scan) != -1) {
                                notmatch = "yes";
                            }
                            
                            if(curR!=getR || curG!=getG || curB!=getB){
                                notmatch = "yes";
                            }
                        }                        
                    }                    
                }                
                if(notmatch == ''){
                    isprocessed = "yes";
                    lastprocess = "4";
                    completed_scan.push(mainIndex);
                    tempIndexes.forEach(function(index){
                        if (jQuery.inArray(index, completed_scan) == -1) {
                            completed_scan.push(index);
                        }                        
                    });
                    count4++;
                    common_logic(global_data, k, x+1, y+1, "2x6 vertical", curR, curG, curB, colorname,2,6);
                }
            }
            
            // 1x8 Horizontal check for each pixel
            if(imgData_new.width-8 >= x && lastprocess!="5" && isprocessed == ""){
                var notmatch = "";
                var tempIndexes = [];
                for(var newy=y; newy < y+1; newy++){
                    for(var newx=x; newx<=x+7; newx++){                        
                        if(newx!=x || newy!=y){                            
                            var currentIndex = imageIndex(imgData_new, newx, newy);
                            tempIndexes.push(currentIndex);
                            var find_clr = getColorAtindex(imgData_new, newx, newy);
                            var getR = find_clr.red;
                            var getG = find_clr.green;
                            var getB = find_clr.blue;
                            
                            if (jQuery.inArray(currentIndex, completed_scan) != -1) {
                                notmatch = "yes";
                            }
                            
                            if(curR!=getR || curG!=getG || curB!=getB){
                                notmatch = "yes";
                            }
                        }                        
                    }                    
                }
                
                if(notmatch == ''){
                    isprocessed = "yes";
                    lastprocess = "5";
                    completed_scan.push(mainIndex);
                    tempIndexes.forEach(function(index){
                        if (jQuery.inArray(index, completed_scan) == -1) {
                            completed_scan.push(index);
                        }                        
                    });
                    count5++;
                    common_logic(global_data, k, x+1, y+1, "1x8 horizontal", curR, curG, curB, colorname,8,1);
                }
            }
            
            // 1x8 Vertical check for each pixel
            if(imgData_new.height-7 >= y && lastprocess!="6" && isprocessed == ""){                
                var notmatch = "";
                var tempIndexes = [];
                for(var newx=x; newx < x+1; newx++){
                    for(var newy=y; newy<=y+7; newy++){                        
                        if(newy!=y){                            
                            var currentIndex = imageIndex(imgData_new, newx, newy);
                            tempIndexes.push(currentIndex);
                            var find_clr = getColorAtindex(imgData_new, newx, newy);
                            var getR = find_clr.red;
                            var getG = find_clr.green;
                            var getB = find_clr.blue;
                            
                            if (jQuery.inArray(currentIndex, completed_scan) != -1) {
                                notmatch = "yes";
                            }
                            
                            if(curR!=getR || curG!=getG || curB!=getB){
                                notmatch = "yes";
                            }
                        }                        
                    }                    
                }
                
                if(notmatch == ''){
                    isprocessed = "yes";
                    lastprocess = "6";
                    completed_scan.push(mainIndex);
                    tempIndexes.forEach(function(index){
                        if (jQuery.inArray(index, completed_scan) == -1) {
                            completed_scan.push(index);
                        }                        
                    });
                    count6++;
                    common_logic(global_data, k, x+1, y+1, "1x8 vertical", curR, curG, curB, colorname,1,8);
                }
            }
            
            // 1x6 Horizontal check for each pixel
            if(imgData_new.width-5 >= x && lastprocess!="7" && isprocessed == ""){
                var notmatch = "";
                var tempIndexes = [];
                for(var newy=y; newy < y+1; newy++){
                    for(var newx=x; newx<=x+5; newx++){                        
                        if(newx!=x || newy!=y){                            
                            var currentIndex = imageIndex(imgData_new, newx, newy);
                            tempIndexes.push(currentIndex);
                            var find_clr = getColorAtindex(imgData_new, newx, newy);
                            var getR = find_clr.red;
                            var getG = find_clr.green;
                            var getB = find_clr.blue;
                            
                            if (jQuery.inArray(currentIndex, completed_scan) != -1) {
                                notmatch = "yes";
                            }
                            
                            if(curR!=getR || curG!=getG || curB!=getB){
                                notmatch = "yes";
                            }
                        }                        
                    }                    
                }
                
                if(notmatch == ''){
                    isprocessed = "yes";
                    lastprocess = "7";
                    completed_scan.push(mainIndex);
                    tempIndexes.forEach(function(index){
                        if (jQuery.inArray(index, completed_scan) == -1) {
                            completed_scan.push(index);
                        }                        
                    });
                    count7++;
                    common_logic(global_data, k, x+1, y+1, "1x6 horizontal", curR, curG, curB, colorname,6,1);
                }
            }
            
            // 1x6 Vertical check for each pixel
            if(imgData_new.height-5 >= y && lastprocess!="8" && isprocessed == ""){
                var notmatch = "";
                var tempIndexes = [];
                for(var newx=x; newx < x+1; newx++){
                    for(var newy=y; newy<=y+5; newy++){                        
                        if(newy!=y){                            
                            var currentIndex = imageIndex(imgData_new, newx, newy);
                            tempIndexes.push(currentIndex);
                            var find_clr = getColorAtindex(imgData_new, newx, newy);
                            var getR = find_clr.red;
                            var getG = find_clr.green;
                            var getB = find_clr.blue;
                            
                            if (jQuery.inArray(currentIndex, completed_scan) != -1) {
                                notmatch = "yes";
                            }                            
                            if(curR!=getR || curG!=getG || curB!=getB){
                                notmatch = "yes";
                            }
                        }                        
                    }                    
                }
                
                if(notmatch == ''){
                    isprocessed = "yes";
                    lastprocess = "8";
                    completed_scan.push(mainIndex);
                    tempIndexes.forEach(function(index){
                        if (jQuery.inArray(index, completed_scan) == -1) {
                            completed_scan.push(index);
                        }                        
                    });
                    count8++;
                    common_logic(global_data, k, x+1, y+1, "1x6 vertical", curR, curG, curB, colorname,1,6);
                }
            }
            
            
            // 2x4 Horizontal check for each pixel
            if(imgData_new.width-3 >= x && imgData_new.height-1 >= y && lastprocess!="9" && isprocessed == ""){
                var notmatch = "";
                var tempIndexes = [];
                for(var newy=y; newy <= y+1; newy++){
                    for(var newx=x; newx<=x+3; newx++){                        
                        if(newx!=x || newy!=y){                            
                            var currentIndex = imageIndex(imgData_new, newx, newy);
                            tempIndexes.push(currentIndex);
                            var find_clr = getColorAtindex(imgData_new, newx, newy);
                            var getR = find_clr.red;
                            var getG = find_clr.green;
                            var getB = find_clr.blue;
                            
                            if (jQuery.inArray(currentIndex, completed_scan) != -1) {
                                notmatch = "yes";
                            }
                            
                            if(curR!=getR || curG!=getG || curB!=getB){
                                notmatch = "yes";
                            }
                        }                        
                    }                    
                }
                
                if(notmatch == ''){
                    isprocessed = "yes";
                    lastprocess = "9";
                    completed_scan.push(mainIndex);
                    tempIndexes.forEach(function(index){
                        if (jQuery.inArray(index, completed_scan) == -1) {
                            completed_scan.push(index);
                        }                        
                    });
                    count9++;
                    common_logic(global_data, k, x+1, y+1, "2x4 horizontal", curR, curG, curB, colorname,4,2);
                }
            }            
            // 2x4 Vertical check for each pixel
            if(imgData_new.height-3 >= y && imgData_new.width-1 >= x && lastprocess!="10" && isprocessed == ""){
                var notmatch = "";
                var tempIndexes = [];
                for(var newx=x; newx <= x+1; newx++){
                    for(var newy=y; newy<=y+3; newy++){                        
                        if(newx!=x || newy!=y){                            
                            var currentIndex = imageIndex(imgData_new, newx, newy);
                            tempIndexes.push(currentIndex);
                            var find_clr = getColorAtindex(imgData_new, newx, newy);
                            var getR = find_clr.red;
                            var getG = find_clr.green;
                            var getB = find_clr.blue;
                            
                            if (jQuery.inArray(currentIndex, completed_scan) != -1) {
                                notmatch = "yes";
                            }
                            
                            if(curR!=getR || curG!=getG || curB!=getB){
                                notmatch = "yes";
                            }
                        }                        
                    }                    
                }
                
                if(notmatch == ''){
                    isprocessed = "yes";
                    lastprocess = "10";
                    completed_scan.push(mainIndex);
                    tempIndexes.forEach(function(index){
                        if (jQuery.inArray(index, completed_scan) == -1) {
                            completed_scan.push(index);
                        }                        
                    });
                    count10++;
                    common_logic(global_data, k, x+1, y+1, "2x4 vertical", curR, curG, curB, colorname,2,4);
                }
            }
            
            // 2x2 check for each pixel
            if(imgData_new.width-1 >= x && imgData_new.height-1 >= y && lastprocess!="11" && isprocessed == ""){
                var notmatch = "";
                var tempIndexes = [];
                for(var newy=y; newy <= y+1; newy++){
                    for(var newx=x; newx<=x+1; newx++){                        
                        if(newx!=x || newy!=y){                            
                            var currentIndex = imageIndex(imgData_new, newx, newy);
                            tempIndexes.push(currentIndex);
                            var find_clr = getColorAtindex(imgData_new, newx, newy);
                            var getR = find_clr.red;
                            var getG = find_clr.green;
                            var getB = find_clr.blue;
                            
                            if (jQuery.inArray(currentIndex, completed_scan) != -1) {
                                notmatch = "yes";
                            }
                            
                            if(curR!=getR || curG!=getG || curB!=getB){
                                notmatch = "yes";
                            }
                        }                        
                    }                    
                }
                
                if(notmatch == ''){
                    isprocessed = "yes";
                    lastprocess = "11";
                    completed_scan.push(mainIndex);
                    tempIndexes.forEach(function(index){
                        if (jQuery.inArray(index, completed_scan) == -1) {
                            completed_scan.push(index);
                        }                        
                    });
                    count11++;
                    common_logic(global_data, k, x+1, y+1, "2x2", curR, curG, curB, colorname,2,2);
                }
            }
            
            
            // 1x4 Horizontal check for each pixel
            if(imgData_new.width-3 >= x && lastprocess!="12" && isprocessed == ""){
                var notmatch = "";
                var tempIndexes = [];
                for(var newy=y; newy < y+1; newy++){
                    for(var newx=x; newx<=x+3; newx++){                        
                        if(newx!=x || newy!=y){                            
                            var currentIndex = imageIndex(imgData_new, newx, newy);
                            tempIndexes.push(currentIndex);
                            var find_clr = getColorAtindex(imgData_new, newx, newy);
                            var getR = find_clr.red;
                            var getG = find_clr.green;
                            var getB = find_clr.blue;
                            
                            if (jQuery.inArray(currentIndex, completed_scan) != -1) {
                                notmatch = "yes";
                            }
                            
                            if(curR!=getR || curG!=getG || curB!=getB){
                                notmatch = "yes";
                            }
                        }                        
                    }                    
                }
                
                if(notmatch == ''){
                    isprocessed = "yes";
                    lastprocess = "12";
                    completed_scan.push(mainIndex);
                    tempIndexes.forEach(function(index){
                        if (jQuery.inArray(index, completed_scan) == -1) {
                            completed_scan.push(index);
                        }                        
                    });
                    count12++;
                    common_logic(global_data, k, x+1, y+1, "1x4 horizontal", curR, curG, curB, colorname,4,1);
                }
            }
            
            
            // 1x4 Vertical check for each pixel
            if(imgData_new.height-3 >= y && lastprocess!="13" && isprocessed == ""){
                var notmatch = "";
                var tempIndexes = [];
                for(var newx=x; newx < x+1; newx++){
                    for(var newy=y; newy<=y+3; newy++){
                        if(newx!=x || newy!=y){                            
                            var currentIndex = imageIndex(imgData_new, newx, newy);
                            tempIndexes.push(currentIndex);
                            var find_clr = getColorAtindex(imgData_new, newx, newy);
                            var getR = find_clr.red;
                            var getG = find_clr.green;
                            var getB = find_clr.blue;
                            
                            if (jQuery.inArray(currentIndex, completed_scan) != -1) {
                                notmatch = "yes";
                            }
                            
                            if(curR!=getR || curG!=getG || curB!=getB){
                                notmatch = "yes";
                            }
                        }                        
                    }                    
                }
                
                if(notmatch == ''){
                    isprocessed = "yes";
                    lastprocess = "13";
                    completed_scan.push(mainIndex);
                    tempIndexes.forEach(function(index){
                        if (jQuery.inArray(index, completed_scan) == -1) {
                            completed_scan.push(index);
                        }                        
                    });
                    count13++;
                    common_logic(global_data, k, x+1, y+1, "1x4 vertical", curR, curG, curB, colorname,1,4);
                }
            }
            
            // 1x2 Horizontal check for each pixel
            if(imgData_new.width-1 >= x && lastprocess!="14" && isprocessed == ""){
                var notmatch = "";
                var tempIndexes = [];
                for(var newy=y; newy < y+1; newy++){
                    for(var newx=x; newx<=x+1; newx++){                        
                        if(newx!=x || newy!=y){
                            
                            var currentIndex = imageIndex(imgData_new, newx, newy);
                            tempIndexes.push(currentIndex);
                            var find_clr = getColorAtindex(imgData_new, newx, newy);
                            var getR = find_clr.red;
                            var getG = find_clr.green;
                            var getB = find_clr.blue;
                            
                            if (jQuery.inArray(currentIndex, completed_scan) != -1) {
                                notmatch = "yes";
                            }
                            
                            if(curR!=getR || curG!=getG || curB!=getB){
                                notmatch = "yes";
                            }
                        }                        
                    }                    
                }
                
                if(notmatch == ''){
                    isprocessed = "yes";
                    lastprocess = "14";
                    completed_scan.push(mainIndex);
                    tempIndexes.forEach(function(index){
                        if (jQuery.inArray(index, completed_scan) == -1) {
                            completed_scan.push(index);
                        }                        
                    });
                    count14++;
                    common_logic(global_data, k, x+1, y+1, "1x2 horizontal", curR, curG, curB, colorname,2,1);
                }
            }
            
            // 1x2 Vertical check for each pixel
            if(imgData_new.height-1 >= y && lastprocess!="15" && isprocessed == ""){
                var notmatch = "";
                var tempIndexes = [];
                for(var newx=x; newx < x+1; newx++){
                    for(var newy=y; newy<=y+1; newy++){                        
                        if(newx!=x || newy!=y){                            
                            var currentIndex = imageIndex(imgData_new, newx, newy);
                            tempIndexes.push(currentIndex);
                            var find_clr = getColorAtindex(imgData_new, newx, newy);
                            var getR = find_clr.red;
                            var getG = find_clr.green;
                            var getB = find_clr.blue;
                            
                            if (jQuery.inArray(currentIndex, completed_scan) != -1) {
                                notmatch = "yes";
                            }
                            
                            if(curR!=getR || curG!=getG || curB!=getB){
                                notmatch = "yes";
                            }
                        }                        
                    }                    
                }
                
                if(notmatch == ''){
                    isprocessed = "yes";
                    lastprocess = "15";
                    completed_scan.push(mainIndex);
                    tempIndexes.forEach(function(index){
                        if (jQuery.inArray(index, completed_scan) == -1) {
                            completed_scan.push(index);
                        }                        
                    });
                    count15++;
                    common_logic(global_data, k, x+1, y+1, "1x2 vertical", curR, curG, curB, colorname,1,2);                    
                }
            }
            
            // 1x1 check for each pixel
            if(isprocessed == ""){
                isprocessed = "yes";
                lastprocess = "16";
                count16++;
                completed_scan.push(mainIndex);
                common_logic(global_data, k, x+1, y+1, "1x1", curR, curG, curB, colorname,1,1);
            }            
            k++;
        }
    }
    
    function common_logic(global_data, k, current_x, current_y, brick_type, curR, curG, curB, color,position_x,position_y){        
        global_data[k] = {
            current_x: current_x,
            current_y: current_y,
            brick_type: brick_type,
            curR: curR,
            curG: curG,
            curB: curB,
            color: color,
            position_x: position_x,
            position_y: position_y
        };
    }
    
    //var csvContent = "data:text/csv;charset=utf-8,";
    var csvContent;
    csvContent = "Pixel position horizontal,Pixel position vertical,Brick type,Colour,Colour-R,Colour-G,Colour-B," + "\r\n";
    global_data.forEach(function(rowArray) {        
        var row = rowArray.current_x+','+rowArray.current_y+','+rowArray.brick_type+','+rowArray.color+','+rowArray.curR+','+rowArray.curG+','+rowArray.curB+',';
        csvContent += row + "\r\n";
    });
    
    jQuery("#byp_csv_data").attr('data-json',csvContent);

    var download = document.getElementById('a_csv');
    download.setAttribute('href', 'data:text/csv;charset=utf-8,' + encodeURIComponent(csvContent));
    download.setAttribute('download', 'image_bricks_sample.csv');
    
    /*var encodedUri = encodeURI(csvContent);
    var link = document.createElement("a");
    link.setAttribute("href", encodedUri);
    link.setAttribute("download", "image_bricks.csv");
    document.body.appendChild(link); // Required for FF

    link.click(); // This will download the data file named "my_data.csv".*/


        setTimeout(function() {
          var i,
            j = 0,
            k = 0,
            l = 0,
            final_x = 0,
            final_y = 0;

          var approx_width = Math.round(imgData.width / newwidth);
          var maxwidth = Math.round(approx_width * newwidth) - 1;

          var approx_height = Math.round(imgData.height / newheight);
          var maxheight = Math.round(approx_height * newheight) - 1;

          for (var y = 0; y < imgData.height; y++) {
            if (y % newheight == 0) {
              final_y = y / newheight;
            }
            for (var x = 0; x < imgData.width; x++) {
              if (x % newwidth == 0) {
                final_x = x / newwidth;
              }
              var idx = imageIndex(imgData_new, final_x, final_y);
              var pix = imgData_new.data;
              var red = pix[idx];
              var green = pix[idx + 1];
              var blue = pix[idx + 2];

              imgData.data[j] = red;
              imgData.data[j + 1] = green;
              imgData.data[j + 2] = blue;
              imgData.data[j + 3] = 255;

              if (maxwidth < x || maxheight < y) {
                imgData.data[j] = 255;
                imgData.data[j + 1] = 255;
                imgData.data[j + 2] = 255;
                imgData.data[j + 3] = 255;
              }

              j = j + 4;
            }
          }

          context.putImageData(imgData, 0, 0);
          
          global_data.forEach(function(rowArray) {
            currentx = rowArray.current_x;
            currenty=rowArray.current_y;
            position_x=rowArray.position_x;
            position_y=rowArray.position_y;

            current_x=(currentx-1)*newwidth;
            current_y=(currenty-1)*newheight;

            for(x=current_x;x<current_x+(position_x*newwidth);x++){
                for(y=current_y;y<current_y+(position_y*newheight);y++){
                    if(y==current_y || y==current_y+(position_y*newheight)-1){
                        var currentIndex = imageIndex(imgData, x, y);
                        imgData.data[currentIndex] = 0;
                        imgData.data[currentIndex + 1] = 0;
                        imgData.data[currentIndex + 2] = 0;
                        imgData.data[currentIndex + 3] = 255;
                    }else if(x==current_x || x == current_x+(position_x*newwidth)-1){
                        var currentIndex = imageIndex(imgData, x, y);                            
                        imgData.data[currentIndex] = 0;
                        imgData.data[currentIndex + 1] = 0;
                        imgData.data[currentIndex + 2] = 0;
                        imgData.data[currentIndex + 3] = 255;
                    }
                }
            }
        });
        context.putImageData(imgData, 0, 0);

          jQuery(".selective-section").css("cursor", "pointer");
          jQuery(".byp-overlay").hide();
          jQuery(".selective-section").css("cursor", "pointer");
          jQuery(".selective-section")
            .find("*")
            .prop("disabled", false);
          jQuery(
            ".crop_ratio_list, .radio-custom, .range, .transform-box button"
          ).removeClass("byp_no_click");
          jQuery(".loader").hide();
        }, 500);
      }

      ////### Applied Floyd–Steinberg dithering algorithm logic to getting the Error data to distribute to neighbour ###///
      function distributeError(img, x, y, errR, errG, errB) {
        addError(img, 7 / 32.0, x + 1, y, errR, errG, errB);
        addError(img, 3 / 32.0, x - 1, y + 1, errR, errG, errB);
        addError(img, 5 / 32.0, x, y + 1, errR, errG, errB);
        addError(img, 1 / 32.0, x + 1, y + 1, errR, errG, errB);
      }

      function addError(img, factor, x, y, errR, errG, errB) {
        if (x < 0 || x >= img.width || y < 0 || y >= img.height) return;
        var clr = getColorAtindex(img, x, y);

        var r = clr.red;
        var g = clr.green;
        var b = clr.blue;

        var setRed = r + errR * factor;
        var setGreen = g + errG * factor;
        var setBlue = b + errB * factor;

        var clr = {
          red: setRed,
          green: setGreen,
          blue: setBlue
        };

        setColorAtIndex(img, x, y, clr);
      }

      /////### Getting the RGB color code for given pixel index ###//////
      function getColorAtindex(img, x, y) {
        var idx = imageIndex(img, x, y);
        var pix = img.data;
        var red = pix[idx];
        var green = pix[idx + 1];
        var blue = pix[idx + 2];
        return {
          red: red,
          green: green,
          blue: blue
        };
      }

      /////### Set the new RGB color code to given pixel index ###//////
      function setColorAtIndex(img, x, y, clr) {
        var idx = imageIndex(img, x, y);
        var pix = img.data;
        pix[idx] = clr.red;
        pix[idx + 1] = clr.green;
        pix[idx + 2] = clr.blue;
      }

      ////## Gettting image index based on x(width) and Y (Height)
      function imageIndex(img, x, y) {
        return 4 * (x + y * img.width);
      }

      function hexToRgb(hex) {
        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result
          ? {
              r: parseInt(result[1], 16),
              g: parseInt(result[2], 16),
              b: parseInt(result[3], 16)
            }
          : null;
      }

      function colorDifference(r1, g1, b1, r2, g2, b2) {
        var sumOfSquares = 0;

        sumOfSquares += Math.pow(r1 - r2, 2);
        sumOfSquares += Math.pow(g1 - g2, 2);
        sumOfSquares += Math.pow(b1 - b2, 2);

        return Math.sqrt(sumOfSquares);
      }

      pixelate(this);
    });

    return this;
  };
})(jQuery);

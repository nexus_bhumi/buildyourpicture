<?php

/**

 * The template for displaying the footer

 *

 * Contains footer content and the closing of the #main and #page div elements.

 *

 * @package WordPress

 * @subpackage TemplateMela

 * @since TemplateMela 1.0

 */

?>

<?php tmpmela_content_after(); ?>

</div>

<!-- .main-content-inner -->

</div>

<!-- .main_inner -->

</div>

<!-- #main -->

<div data-vc-full-width="true" data-vc-full-width-init="true" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid theme-container btmsrvcicn vc_custom_1560343994913 vc_row-has-fill vc_row-no-padding vc_row-o-content-middle vc_row-flex" style="position: relative; left: 0px; box-sizing: border-box; width: 1349px;">
    <?php if( have_rows('pre_footer_icons', 212) ): ?>
  <?php while( have_rows('pre_footer_icons', 212) ): the_row();
      $font_awesome_icon = get_sub_field('font_awesome_icon');
      $pre_footer_title = get_sub_field('pre_footer_title');
       ?>
   <div class="wpb_column vc_column_container vc_col-sm-2">
      <div class="vc_column-inner">
         <div class="wpb_wrapper">
            <div class="service hb-animate-element bottom-to-top style-2">
               <div class="service-content style-2">
                  <div class="icon"><i class="service-icon fa <?php echo $font_awesome_icon; ?>" style="color:#c42228;background-color:#ffffff;"></i></div>
                  <div class="title service-text"><a href="" target=""><?php echo $pre_footer_title; ?></a></div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <?php endwhile; ?>
    <?php endif; ?>

</div>

<?php tmpmela_footer_before(); ?>

<footer id="colophon" class="site-footer">	

	 <div class="theme-container">

		 <?php tmpmela_footer_inside(); ?>

		<?php get_sidebar('footer'); ?>

	 <div class="footer-bottom">	

			   <div class="site-info">  
			   	<?php if ( is_active_sidebar( 'foo_copyright' ) ) : ?>
				    <?php dynamic_sidebar( 'foo_copyright' ); ?>
			    <?php endif; ?>
					  </div>

			</div>

		</div>

</footer>

<!-- #colophon -->

<?php tmpmela_footer_after(); ?>

</div>

<!-- #page -->

<?php tmpmela_go_top(); ?>

<?php tmpmela_get_widget('before-end-body-widget'); ?>

<script>
jQuery(document).ready(function() {
        jQuery(".hide-href a").removeAttr("href");

       /* jQuery('.wonderplugin-gridgallery-list').mCustomScrollbar({
	    theme:"dark-3", 	    
	}); */

});
</script>

<?php wp_footer(); ?>

</body></html>